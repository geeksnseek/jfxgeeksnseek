/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author debba
 */
public class DB {
      private final static String url = "jdbc:mysql://localhost:3306/assosymfo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Paris";
    private final static String username = "root";
    private final static String password = "";
    private static Connection connection;
    private static DB instance;

    private DB()
    {
        try {
            if (connection == null) {
                connection = DriverManager.getConnection(url, username, password);
                System.out.println("Connection successful");

            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection getConnection() {

        return connection;
    }

    public static DB getInstance() {
        if (instance == null) {
            instance = new DB();
        }
        return instance;

    } 
}
