/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.InputStream;
import java.sql.Date;
import java.util.stream.Stream;

/**
 *
 * @author med
 */
public class Association {
    private  int id;
    private  String nom_association;
    private Date date_creation;
    private String adresse;
    private double capital;
    private int id_chef;
    private InputStream image;
    public Association() {
    }
    

    public Association(String nom_association,double capital) {
        this.nom_association = nom_association;
        //this.date_creation = date_creation;
        this.capital=capital;
    }

    public Association(int id, String nom_association, Date date_creation, String adresse, double capital , int idchef , InputStream img) {
        this.id = id;
        this.nom_association = nom_association;
        this.date_creation = date_creation;
        this.adresse = adresse;
        this.capital = capital;
        this.id_chef= idchef;
        this.image = img;
    }

    public Association(String nom_association, String adresse, double capital, int idchef,InputStream img) {
        this.nom_association = nom_association;
        this.adresse = adresse;
        this.capital = capital;
        this.id_chef = idchef;
        this.image = img;
    }

    public Association(int id, String nom_association, Date date_creation, String adresse, double capital, int id_chef) {
        this.id = id;
        this.nom_association = nom_association;
        this.date_creation = date_creation;
        this.adresse = adresse;
        this.capital = capital;
        this.id_chef = id_chef;
    }

    
    
    
    

    public int getId() {
        return id;
    }


    public String getNom_association() {
        return nom_association;
    }

    public void setNom_association(String nom_association) {
        this.nom_association = nom_association;
    }

    public Date getDate_creation() {
        return date_creation;
    }

    public void setDate_creation(Date date_creation) {
        this.date_creation = date_creation;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public int getId_chef() {
        return id_chef;
    }

    public void setId_chef(int id_chef) {
        this.id_chef = id_chef;
    }

    public InputStream getImage() {
        return image;
    }

    public void setImage(InputStream image) {
        this.image = image;
    }
    

    @Override
    public String toString() {
        return nom_association ;
    }
    
    
}
