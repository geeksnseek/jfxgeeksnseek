/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import java.util.Objects;
import javafx.collections.ObservableList;

/**
 *
 * @author debba
 */
public class Evenement {
     private int id;
 private String nom;
 private String type;
 private Date date;
 private String lieu;
 private String description;
 private String heure;
  private String img;
private int idM;
    public Evenement() {
    }

            

    public Evenement(int id, String nom, String type, Date date, String lieu, String description, String heure, String img, int idM) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.img = img;
        this.idM = idM;
    }

    public Evenement(String nom, String type, Date date, String lieu, String description, String heure, String img, int idM) {
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.img = img;
        this.idM = idM;
    }

   

    public Evenement(int id, String nom, String type, Date date, String lieu, String description, String heure, String img) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.img = img;
    }

    public Evenement(int id, String nom, String type, Date date, String lieu, String description, String heure) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
    }

    public Evenement(String nom, String type, Date date, String lieu, String description, String heure) {
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
    }

    public Evenement(String nom, String type, Date date, String lieu, String description, String heure, String img) {
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.img = img;
    }

    public Evenement(int id, int idM) {
        this.id = id;
        this.idM = idM;
    }

   



    public int getIdM() {
        return idM;
    }

    public void setIdM(int idM) {
        this.idM = idM;
    }
    
 public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id=" + id + ", nom=" + nom + ", type=" + type + ", date=" + date + ", lieu=" + lieu + ", description=" + description + ", heure=" + heure + ", img=" + img + ", idM=" + idM + '}';
    }

   
   

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.id;
        hash = 47 * hash + Objects.hashCode(this.nom);
        hash = 47 * hash + Objects.hashCode(this.type);
        hash = 47 * hash + Objects.hashCode(this.date);
        hash = 47 * hash + Objects.hashCode(this.lieu);
        hash = 47 * hash + Objects.hashCode(this.description);
        hash = 47 * hash + Objects.hashCode(this.heure);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Evenement other = (Evenement) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.lieu, other.lieu)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.heure, other.heure)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    
   

  
    
    

}
