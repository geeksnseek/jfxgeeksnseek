/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author debba
 */
public class Membre {

    private int id;
    private String nom;
    private String prenom;
    private String sexe;
    private Date datenaissance;
    private String email;
    private String adresse;
    private int telephone;
    private String nomutilisateur;
    private String motdepasse;
    private String grade;

    

    public Membre() {
    }

    public Membre( int id ,String nom, String prenom, String sexe, Date datenaissance, String email, String adresse, int telephone, String nomutilisateur, String motdepasse,String grade) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.datenaissance = datenaissance;
        this.email = email;
        this.adresse = adresse;
        this.telephone = telephone;
        this.nomutilisateur = nomutilisateur;
        this.motdepasse = motdepasse;
        this.grade = grade;
    }
    public Membre( String nom, String prenom, String sexe, Date datenaissance, String email, String adresse, int telephone, String nomutilisateur, String motdepasse,String grade) {
        
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.datenaissance = datenaissance;
        this.email = email;
        this.adresse = adresse;
        this.telephone = telephone;
        this.nomutilisateur = nomutilisateur;
        this.motdepasse = motdepasse;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSex() {
        return sexe;
    }

    public void setSex(String cin) {
        this.sexe = cin;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getNomutilisateur() {
        return nomutilisateur;
    }

    public void setNomutilisateur(String nomutilisateur) {
        this.nomutilisateur = nomutilisateur;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }
 public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.id;
        hash = 59 * hash + Objects.hashCode(this.nom);
        hash = 59 * hash + Objects.hashCode(this.prenom);
        hash = 59 * hash + Objects.hashCode(this.sexe);
        hash = 59 * hash + Objects.hashCode(this.datenaissance);
        hash = 59 * hash + Objects.hashCode(this.email);
        hash = 59 * hash + Objects.hashCode(this.adresse);
        hash = 59 * hash + this.telephone;
        hash = 59 * hash + Objects.hashCode(this.nomutilisateur);
        hash = 59 * hash + Objects.hashCode(this.motdepasse);
        hash = 59 * hash + Objects.hashCode(this.grade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Membre other = (Membre) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.telephone != other.telephone) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.sexe, other.sexe)) {
            return false;
        }
        if (!Objects.equals(this.datenaissance, other.datenaissance)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.nomutilisateur, other.nomutilisateur)) {
            return false;
        }
        if (!Objects.equals(this.motdepasse, other.motdepasse)) {
            return false;
        }
        if (!Objects.equals(this.grade, other.grade)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Membre{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", sexe=" + sexe + ", datenaissance=" + datenaissance + ", email=" + email + ", adresse=" + adresse + ", telephone=" + telephone + ", nomutilisateur=" + nomutilisateur + ", motdepasse=" + motdepasse + ", grade=" + grade + '}';
    }
    
}
