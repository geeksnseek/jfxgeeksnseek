/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;








/**
 *
 * @author brini
 */
public class Panier 
{
    
    private int idcmd ;
    private int idmemebre ;
    private int idcat ;
    private double quantitedes ;
    private double prix ;
    private double totalprix ;
    private Date date ;
    private String description ;
    
    public Panier()
    {
        
    }

    public Panier( String description,double quantitedes, double prix, Date date) 
    {
        this.description = description;
        this.quantitedes = quantitedes;
        this.prix = prix;
        this.date = date;
      
    }

    public Panier(int idcmd, int idcat,String description, double quantitedes, double prix, double totalprix) {
        this.idcmd = idcmd;
        this.idcat = idcat;
        this.quantitedes = quantitedes;
        this.prix = prix;
        this.totalprix = totalprix;
        this.description = description;
    }

 
    public Panier(int idcmd, int idmemebre, int idcat, double quantitedes, double prix)
    {
        this.idcmd = idcmd;
        this.idmemebre = idmemebre;
        this.idcat = idcat;
        this.quantitedes = quantitedes;
        this.prix = prix;
    }
      public Panier( int idmemebre, int idcat, double quantitedes, double prix)
    {
      
        this.idmemebre = idmemebre;
        this.idcat = idcat;
        this.quantitedes = quantitedes;
        this.prix = prix;
    }

    public Panier(int idcmd, int idcat, double quantitedes, double prix, double totalprix) {
        this.idcmd = idcmd;
        this.idcat = idcat;
        this.quantitedes = quantitedes;
        this.prix = prix;
        this.totalprix = totalprix;
    }
    
    

    public Panier(int id ,String descripition ,int idcmd, double quantitedes, double prix ,Date date)
    {   this.idcat=id ;
        this.idcmd = idcmd;
        this.description = description;
        this.quantitedes = quantitedes;
        this.prix = prix;
        this.date = date ;
       
       
    }
   
       
       
    
    
    
    

    public int getIdcmd() {
        return idcmd;
    }

    public void setIdcmd(int idcmd) {
        this.idcmd = idcmd;
    }

    public int getIdmemebre() {
        return idmemebre;
    }

    public void setIdmemebre(int idmemebre) {
        this.idmemebre = idmemebre;
    }

    public int getIdcat() {
        return idcat;
    }

    public void setIdcat(int idcat) {
        this.idcat = idcat;
    }

    public double getQuantitedes() {
        return quantitedes;
    }

    public void setQuantitedes(double quantitedes) {
        this.quantitedes = quantitedes;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public double getTotalprix() {
        return totalprix;
    }

    public void setTotalprix(double totalprix) {
        this.totalprix = totalprix;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description) 
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return "Panier{" + " description= " + description + ", quantitedes= " + quantitedes + ", prix= " + prix +",date = "+date+'}';
    }

 
    

 
 
  
    
    
    
    
    
    
}
