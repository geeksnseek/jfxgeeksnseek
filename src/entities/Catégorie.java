/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author brini
 */
public class Catégorie

{
    private int id ;
    private double quantitedispo ;
    private double prixuni ;
    private String description ;
    private String img ;

    public Catégorie(){
        
    }
    public Catégorie(int id,  String description,double quantitedispo, double prixuni)
    {
        this.id = id;
        this.quantitedispo = quantitedispo;
        this.prixuni = prixuni;
        this.description = description;
    }

    public Catégorie(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public Catégorie(int id ,double quantitedispo, double prixuni) {
         this.id = id;
        this.quantitedispo = quantitedispo;
        this.prixuni = prixuni;
    }
    

    
    public Catégorie(double quantitedispo, double prixuni, String description)
    {
        this.quantitedispo = quantitedispo;
        this.prixuni = prixuni;
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Catégorie(String description, String img) {
        this.description = description;
        this.img = img;
    }
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getQuantitedispo() {
        return quantitedispo;
    }

    public void setQuantitedispo(double quantitedispo) {
        this.quantitedispo = quantitedispo;
    }

    public double getPrixuni() {
        return prixuni;
    }

    public void setPrixuni(double prixuni) {
        this.prixuni = prixuni;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
//
//    @Override
//    public String toString() {
//        return "Catégorie {" +" description :" + description +  "quantitedispo =" + quantitedispo + ", prixuni =" + prixuni + '}';
//    }
//    

//    @Override
//    public String toString() {
//        return "Catégorie{" + "id=" + id + ", quantitedispo=" + quantitedispo + ", prixuni=" + prixuni + '}';
//    }

    @Override
    public String toString() {
        return "Catégorie{" + "quantitedispo=" + quantitedispo + ", prixuni=" + prixuni + ", description=" + description + '}';
    }
    
    
}
