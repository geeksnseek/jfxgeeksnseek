/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Objects;

/**
 *
 * @author debba
 */
public class Participant {
   
private int idP;
    private int idM;
    public int idevent;
    public String etat;
    public String avis;

    public Participant() {
    }

    public Participant(int idP, int idM, int idevent, String etat, String avis) {
        this.idP = idP;
        this.idM = idM;
        this.idevent = idevent;
        this.etat = etat;
        this.avis = avis;
    }

    public Participant(int idM, int idevent, String etat, String avis) {
        this.idM = idM;
        this.idevent = idevent;
        this.etat = etat;
        this.avis = avis;
    }

 

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public int getIdM() {
        return idM;
    }

    public void setIdM(int idM) {
        this.idM = idM;
    }

    public int getIdevent() {
        return idevent;
    }

    public void setIdevent(int idevent) {
        this.idevent = idevent;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.idP;
        hash = 97 * hash + this.idM;
        hash = 97 * hash + this.idevent;
        hash = 97 * hash + Objects.hashCode(this.etat);
        hash = 97 * hash + Objects.hashCode(this.avis);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participant other = (Participant) obj;
        if (this.idP != other.idP) {
            return false;
        }
        if (this.idM != other.idM) {
            return false;
        }
        if (this.idevent != other.idevent) {
            return false;
        }
        if (!Objects.equals(this.etat, other.etat)) {
            return false;
        }
        if (!Objects.equals(this.avis, other.avis)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Participant{" + "idP=" + idP + ", idM=" + idM + ", idevent=" + idevent + ", etat=" + etat + ", avis=" + avis + '}';
    }

    
    

    
  

  

}
