/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author PC
 */
public class ControleSaisie {
    
     public boolean GSM(String num) {
        String masque = "[0-9]{8}$";
        Pattern pattern = Pattern.compile(masque);
        Matcher controler = pattern.matcher(num);
        if (controler.matches()) {
            return true;
        }
        return false;
    }
	 
	 
	 public boolean Nbr(String num) {
        String masque = "[0-9]{2}$";
        Pattern pattern = Pattern.compile(masque);
        Matcher controler = pattern.matcher(num);
        if (controler.matches()) {
            return true;
        }
        return false;
    }
     
      public boolean mailformat(String mail) {
        String masque = "^[a-zA-Z]+[a-zA-Z0-9\\._-]*[a-zA-Z0-9]@[a-zA-Z]+"
                + "[a-zA-Z0-9\\._-]*[a-zA-Z0-9]+\\.[a-zA-Z]{2,4}$";
        Pattern pattern = Pattern.compile(masque);
        Matcher controler = pattern.matcher(mail);
        return controler.matches();

    }
    
}
