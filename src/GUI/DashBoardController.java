/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import database.DB;
import static database.DB.getInstance;
import entities.Evenement;
import Utils.session;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import services.ParticipantService;

/**
 *
 * @author debba
 */
public class DashBoardController implements Initializable {
    @FXML
    private BarChart<?, ?> SalaryChart;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;
 @FXML
    private Label Lwelcome;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Lwelcome.setText("Bonjour "+session.getUser());
            ObservableList<Integer> list1 = FXCollections.observableArrayList();
            ObservableList<String> list2 = FXCollections.observableArrayList();
            Connection connection;
            DB db = getInstance();
            connection = db.getConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT count(p.idM) nb,e.nom FROM `participant` p INNER JOIN `evttesting` e on p.idevent=e.id GROUP by idevent" );
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ParticipantService k = new ParticipantService();
                list1.add(rs.getInt(1));
                list2.add(rs.getString(2));
            }
            System.out.println("iiiiiii");
            System.out.println(list1);
            System.out.println(list2);
             XYChart.Series set1=new XYChart.Series<>();
   
             for(int k=0,p=0;k<list2.size()&& p<list1.size();k++,p++){
           
                 System.out.println(k);
             set1.getData().add(new XYChart.Data(list2.get(k), list1.get(p)));
               }
            SalaryChart.getData().addAll(set1);

            
           
        } catch (SQLException ex) {
            
        }
    }
     @FXML
    void Revnir(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);

    }
    ///////Menu
    @FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ViewEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/HomeEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AddEvent.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button4(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Events.fxml")));
        stage.setScene(scene);

    }
 
}
