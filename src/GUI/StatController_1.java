package GUI;

import database.DB;
import static database.DB.getInstance;
import Utils.session;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class StatController_1 implements Initializable {
@FXML
    private BarChart<?, ?> SalaryChart;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;
    @FXML
    private BorderPane borderpane;

    @FXML
    private AnchorPane parent;

    @FXML
    private Label Lhello;

    @FXML
    private TextField TFidea;
   @FXML
    private Pane pane;
    private Stage stage;

    MembreServices ms = new MembreServices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
           pane.setVisible(false);
      SalaryChart.setVisible(false);
        try {
            Lhello.setText("Bonjour " + session.getUser());
            ObservableList<Integer> list1 = FXCollections.observableArrayList();
            ObservableList<String> list2 = FXCollections.observableArrayList();
             ObservableList<Integer> list3 = FXCollections.observableArrayList();
               ObservableList<Integer> list4 = FXCollections.observableArrayList();
            Connection connection;
            DB db = getInstance();
            connection = db.getConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT count(p.idM) nb,e.nom FROM `participant` p INNER JOIN `evttesting` e on p.idevent=e.id GROUP by idevent" );
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ParticipantService k = new ParticipantService();
                list1.add(rs.getInt(1));
                list2.add(rs.getString(2));
            }
            System.out.println("iiiiiii");
            System.out.println(list1);
            System.out.println(list2);
             XYChart.Series set1=new XYChart.Series<>();
   
             for(int k=0,p=0;k<list2.size()&& p<list1.size();k++,p++){
           
                 System.out.println(k);
             set1.getData().add(new XYChart.Data(list2.get(k), list1.get(p)));
               }
            SalaryChart.getData().addAll(set1);

           
            int nbr_mbr = ms.countMembres();
          

            ObservableList<PieChart.Data> list = FXCollections.observableArrayList();
       
            PreparedStatement pss = connection.prepareStatement("SELECT count(idM) FROM `participant`" );
            ResultSet rss = pss.executeQuery();
            while (rs.next()) {
                ParticipantService k = new ParticipantService();
                list3.add(rss.getInt(1));
               
            }
           PreparedStatement psss = connection.prepareStatement("SELECT count(idM) FROM `membre`" );
            ResultSet rsss = psss.executeQuery();
            while (rs.next()) {
                ParticipantService k = new ParticipantService();
                list3.add(rss.getInt(1));
               
            }
                 list.add(new PieChart.Data("Participant",list3.size()));
            list.add(new PieChart.Data("Membre non participant", list4.size() - list3.size()));
            //list.add(new PieChart.Data("test",8));
            PieChart pie = new PieChart(list);
            //pieRadius = Math.min(contentWidth,contentHeight) / 2;

            pane.getChildren().add(pie);
            } catch (SQLException ex) {
            
        }

    }

    @FXML
    private void showpiechart() {
        pane.setVisible(true);
       SalaryChart.setVisible(false);

    }

    @FXML
    private void showhistogramme() {
        pane.setVisible(false);
      SalaryChart.setVisible(true);

    }
    


    ////////Menu
@FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/TousEvents.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Stat.fxml")));
        stage.setScene(scene);

    }

    



    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AdminEvents.fxml")));
        stage.setScene(scene);

    }
         @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("3 nouveaux évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
     notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }
}
