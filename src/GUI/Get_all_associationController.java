package GUI;

import Utils.session;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import entities.Association;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.StageStyle;

public class Get_all_associationController implements Initializable {

    @FXML
    private BorderPane borderpane;

    @FXML
    private Label Lhello;
    @FXML
    private Label Ltest;

    @FXML
    private AnchorPane parent;

    @FXML
    void backToMenu(ActionEvent event) {

    }

    @FXML
    private ListView<HBox> LVlist;

    @FXML
    private Button BTjoin;
    
    @FXML
    private TextField TFrecherche;

    @FXML
    private Button BTaccess;

    private Stage stage;

    MembreServices ms = new MembreServices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {
            Lhello.setText("Bonjour " + session.getUser());
        } catch (Exception e) {
        }
        Associationservices as = new Associationservices();
        try {
            ObservableList<ImageView> ivs = as.getAllAssociationImages();

            ObservableList<Association> als = as.getAllassociations();
            ObservableList<Label> labels = FXCollections.observableArrayList();

            for (int i = 0; i < als.size(); i++) {
                labels.add(new Label(als.get(i).getNom_association()));
            }
            final double MAX_FONT_SIZE = 30.0;

            ObservableList<HBox> hbs = FXCollections.observableArrayList();
            HBox hb = new HBox();
            HBox hb2 = new HBox();
            HBox hb3 = new HBox();

            for (int i = 0; i < ivs.size(); i++) {
                //hb.getChildren().addAll(ivs.get(0),labels.get(0));
                labels.get(i).setFont(new Font(MAX_FONT_SIZE));
                labels.get(i).setPadding(new Insets(70, 0, 0, 0));
                hbs.add(new HBox(ivs.get(i), labels.get(i)));

            }
            for (int i = 0; i < ivs.size(); i++) {
                hbs.get(i).setSpacing(80);
            }
//                Label l2 = new Label();
//                l2.setText(als.get(2).getNom_association());
//                hb2.getChildren().addAll(ivs.get(2),labels.get(2));
//                hbs.add(hb2);
//                
//                hb3.getChildren().addAll(ivs.get(3),l);
//                hbs.add(hb3);

//            LVlist.setItems(as.getAllAssociationImages());
            LVlist.setItems(hbs);

        } catch (SQLException ex) {
            Logger.getLogger(Get_all_associationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Get_all_associationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        TFrecherche.textProperty().addListener((observable,oldvalue,newvalue)->{
            try {
                ObservableList<ImageView> ivs = as.getAllSearshedAssociationImages(TFrecherche.getText());

            ObservableList<Association> als = as.SearshInAllAssociations(TFrecherche.getText());
            ObservableList<Label> labels = FXCollections.observableArrayList();

            for (int i = 0; i < als.size(); i++) {
                labels.add(new Label(als.get(i).getNom_association()));
            }
            final double MAX_FONT_SIZE = 30.0;

            ObservableList<HBox> hbs = FXCollections.observableArrayList();
            HBox hb = new HBox();
            HBox hb2 = new HBox();
            HBox hb3 = new HBox();

            for (int i = 0; i < ivs.size(); i++) {
                //hb.getChildren().addAll(ivs.get(0),labels.get(0));
                labels.get(i).setFont(new Font(MAX_FONT_SIZE));
                labels.get(i).setPadding(new Insets(70, 0, 0, 0));
                hbs.add(new HBox(ivs.get(i), labels.get(i)));

            }
            for (int i = 0; i < ivs.size(); i++) {
                hbs.get(i).setSpacing(80);
            }
                LVlist.setItems(hbs);
            } catch (SQLException ex) {
                Logger.getLogger(User_associationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (IOException ex) {
            Logger.getLogger(Get_all_associationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        });

    }

    @FXML
    private void displaySeleckted() throws SQLException {
        Associationservices as = new Associationservices();
        HBox hb = new HBox();
        hb = LVlist.getSelectionModel().getSelectedItem();
        Node nodeOut = hb.getChildren().get(1);
        if (nodeOut instanceof Label) {

            Ltest.setText(((Label) nodeOut).getText());
            int id = as.getidAssoByName(Ltest.getText());

            if (as.alreadyAdherent(id, ms.getidUSERByusername())) {
                BTjoin.setVisible(false);
            } else {
                BTjoin.setVisible(true);
            }
        }
    }

    @FXML
    private void joinAssociation(ActionEvent event) throws IOException, SQLException {
        Associationservices as = new Associationservices();
        if (Ltest.getText().equals("")) {
            Alert alertu = new Alert(Alert.AlertType.ERROR);
            alertu.setTitle("pas de choix!");
            alertu.setHeaderText("vous dever choisir une association !");
            Optional<ButtonType> result = alertu.showAndWait();
        }
        int id = as.getidAssoByName(Ltest.getText());
        session.setNomasso(Ltest.getText());
        if (!as.alreadyAdherent(id, ms.getidUSERByusername())) {
            as.addadherent(id, ms.getidUSERByusername());
        }
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/inassociation.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button1(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/add_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/user_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3() {
        //current

    }

    @FXML
    void button4(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) parent.getScene().getWindow();
        stagec.close();

    }

}
