package GUI;

import Utils.session;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class Admin_associationController implements Initializable {

    @FXML
    private BorderPane borderpane;

    @FXML
    private AnchorPane parent;

    @FXML
    private Label Lhello;

    @FXML
    private TextField TFidea;

    private Stage stage;

    MembreServices ms = new MembreServices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {
            Lhello.setText("Bonjour " + session.getUser());
        } catch (Exception e) {

        }

    }

    @FXML
    private void publish() throws SQLException {

    }

    @FXML
    void button1(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/admin_associationFeedbacks.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/admin_associationStats.fxml")));
        stage.setScene(scene);

    }

//    @FXML
//    void button3(ActionEvent event) throws IOException {
//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
//LocalDate d;
//LocalDate locald;
//                            //locald = DPdatenaissance.getValue();
//                             date = java.sql.Date.valueOf(locald);
//
//    }
//
//    @FXML
//    void button4(ActionEvent event) throws IOException {
//        Node source = (Node) event.getSource();
//        Stage stage = (Stage) source.getScene().getWindow();
//        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
//        stage.setScene(scene);
//    }

    @FXML
    void backToMenu() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/MenuAdmin.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) parent.getScene().getWindow();
        stagec.close();

    }

}
