/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import entities.Mailtest;
import entities.Membre;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoClientException;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.SmsSubmissionResponseMessage;
import com.nexmo.client.sms.messages.TextMessage;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.controlsfx.control.Notifications;

/**
 *
 * @author debba
 */
public class NewController  implements Initializable{
    
    @FXML
    private Pane paneenv;

    @FXML
    private TextArea contenu;

    @FXML
    private TextField numerooo;

    @FXML
    private JFXButton envoi;

    @FXML
    private Label monemail;

    @FXML
    private Label lt;

    @FXML
    private TextField sujet;

    @FXML
    private Pane p2;

    @FXML
    private JFXRadioButton sms;

    @FXML
    private Label username;

    @FXML
    private Pane p1;

    @FXML
    private JFXRadioButton smail;

    
      Membre connect = new Membre(16, "Emna", "Debbabi", "femme", new Date(1995, 9, 5), "debbabiemna5@gmail.com", "Nabeul", 52978520, "emna5", "emna591995", "Membre");
       Membre owner = new Membre(16, "Emna", "Debbabi", "femme", new Date(1995, 9, 5), "debbabiemna5@gmail.com", "Nabeul", 52978520, "emna5", "emna591995", "Administrateur");
     @Override
    public void initialize(URL url, ResourceBundle rb) {
        username.setText(owner.getEmail());
        // TODO
        paneenv.setVisible(false);       
        monemail.setText(connect.getEmail());
        
//            p1.setVisible(true);
//            
//            p2.setVisible(true);
//            smail.setSelected(false);
//            paneenv.setVisible(false);
//            paneenv.setStyle("-fx-background-color:  #fff5f5;");
//  if (smail.isSelected()) {
//       p2.setVisible(true);
//      paneenv.setVisible(true);}
//   if (smail.isSelected()) {
//       p1.setVisible(false);
//      paneenv.setVisible(true);}
           numerooo.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
                public void handle(KeyEvent t) {
                    char ar[] = t.getCharacter().toCharArray();
                    char ch = ar[t.getCharacter().toCharArray().length - 1];
                    if (!(ch >= '0' && ch <= '9')) {
                        t.consume();
                    }

                    int maxCharacters = 8;
                    if (numerooo.getText().length() > maxCharacters) {
                        t.consume();
                       contenu.setText("nom max 20 c");
                    }
                }
            });
           smail.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(smail.isSelected())
                      
                 paneenv.setVisible(true);
            }
           
           });
           sms.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(sms.isSelected())
                    
                 paneenv.setVisible(true);
            }
           
           });

        }

    
    
    @FXML
    private void envoyer(ActionEvent event) throws IOException, NexmoClientException, AddressException, MessagingException,SQLException {
    
                if(smail.isSelected()){
                      
       if (contenu.getText().length() != 0){         

       try {
           final String username =connect.getEmail();
           final String password =connect.getMotdepasse();
           Properties props = new Properties();
           props.put("mail.smtp.auth", "true");
           props.put("mail.smtp.starttls.enable", "true");
           props.put("mail.smtp.host", "smtp.gmail.com");
           props.put("mail.smtp.port", "587");
           
           Session session = Session.getInstance(props,
                   new javax.mail.Authenticator() {
                       @Override
                       protected PasswordAuthentication getPasswordAuthentication() {
                           return new PasswordAuthentication(username, password);
                       }
                   });
           Message message = new MimeMessage(session);
//        message.setFrom(new InternetAddress("dhiabsdl94@gmail.com"));
message.setFrom(new InternetAddress(connect.getEmail()));
message.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse(owner.getEmail()));

message.setSubject(sujet.getText());
message.setText(contenu.getText());

Transport.send(message);

System.out.println("Done");

Notifications.create().hideAfter(Duration.seconds(2)).position(Pos.CENTER).text("email envoyer").showConfirm();

////////////
       } catch (MessagingException ex) {
          
       }
                    
                   }
else{Notifications.create().hideAfter(Duration.seconds(5)).position(Pos.CENTER).text("email non envoyer verifier votre connection").showError(); 
        }
                }
        
                 
           
           
  if (sms.isSelected()){
  if((numerooo.getText().length()>0)&&(numerooo.getText().length()<8)&&(contenu.getText().length() != 0))
  {
            

 NexmoClient client = new NexmoClient.Builder()
  .apiKey("af38776e")
  .apiSecret("XMxqOZ4zU8tAu4F9")
  .build();

String messageText = contenu.getText();
TextMessage message = new TextMessage("Nexmo", "21652978520", messageText);

SmsSubmissionResponse response = client.getSmsClient().submitMessage(message);

for (SmsSubmissionResponseMessage responseMessage : response.getMessages()) {
    System.out.println(responseMessage);}
Notifications.create().hideAfter(Duration.seconds(2)).position(Pos.CENTER).text("sms envoyer").showConfirm();
}

 else{ Notifications.create().hideAfter(Duration.seconds(5)).position(Pos.CENTER).text("sms non envoyer \n verfier votre connection").showError();}                 

                   } 
        }
       @FXML
    private void numerooosaisie(KeyEvent t ) {
        
       
                    char ar[] = t.getCharacter().toCharArray();
                    char ch = ar[t.getCharacter().toCharArray().length - 1];
                    if (!(ch >= '0' && ch <= '9')) {
                        t.consume();
                    }

       
                    int maxCharacters = 8;
                    if (numerooo.getText().length() > maxCharacters) {
                        t.consume();
                      //  tfd.setText("nom max 8 chiffre");
                    
    }}
    }


