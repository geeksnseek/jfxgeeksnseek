package GUI;

import entities.Evenement;
import Utils.session;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class TousEventsController implements Initializable {

    @FXML
    private BorderPane borderpane;

    @FXML
    private AnchorPane parent;

    @FXML
    private Label Lhello;

    @FXML
    private TextField TFidea;

    private Stage stage;
    @FXML
    private TableView<Evenement> idtable;
    MembreServices ms = new MembreServices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {
            Lhello.setText("Bonjour " + session.getUser());
           // idmodif.setVisible(false);
        ServiceEvent cm = new ServiceEvent();
        TableColumn DesColumn = new TableColumn("Nom");
        TableColumn QteColumn = new TableColumn("Type");
        TableColumn PrixColumn = new TableColumn("Date");
        TableColumn a = new TableColumn("Lieu");
        TableColumn b = new TableColumn("Description");
        TableColumn c= new TableColumn("Heure ");
     
    
            
        idtable.getColumns().addAll(DesColumn,QteColumn,PrixColumn,a,b,c);
        
//        idtable.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
//        DesColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 40 ); 
//        QteColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 ); 
//        PrixColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 );
//          DesColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 40 ); 
//        QteColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 ); 
//        PrixColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 );
//       PrixColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 );
        
        
        DesColumn.setCellValueFactory(new PropertyValueFactory<Evenement,String>("nom"));    
        QteColumn.setCellValueFactory(new PropertyValueFactory<Evenement,String>("type"));
        PrixColumn.setCellValueFactory(new PropertyValueFactory<Evenement,Date>("date"));    
          a.setCellValueFactory(new PropertyValueFactory<Evenement,String>("lieu"));    
      b.setCellValueFactory(new PropertyValueFactory<Evenement,String>("description"));
        c.setCellValueFactory(new PropertyValueFactory<Evenement,String>("heure"));  
        
       
        idtable.setItems(cm.getAll());
        } catch (Exception e) {

        }

    }

    @FXML
    private void Afficher() throws SQLException {

    }

    
//    @FXML
//    void button3(ActionEvent event) throws IOException {
//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
//LocalDate d;
//LocalDate locald;
//                            //locald = DPdatenaissance.getValue();
//                             date = java.sql.Date.valueOf(locald);
//
//    }
//
//    @FXML
//    void button4(ActionEvent event) throws IOException {
//        Node source = (Node) event.getSource();
//        Stage stage = (Stage) source.getScene().getWindow();
//        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
//        stage.setScene(scene);
//    }

    ////////Menu
@FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/TousEvents.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Stat.fxml")));
        stage.setScene(scene);

    }

    



    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/MenuAdmin.fxml")));
        stage.setScene(scene);

    }
         @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("3 nouveaux évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
     notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }
}
