/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import database.DB;
import entities.Evenement;
import entities.Membre;

import static GUI.ViewEventController.ev;
import Utils.session;
import java.awt.RenderingHints;
import services.ServiceEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import services.MembreServices;

/**
 *
 * @author debba
 */
public class HomeEventController implements Initializable {

   

   @FXML
    private Label Lwelcome;
   
    @FXML
    private ListView<Evenement> listEvents;

    public static Evenement ev;
    MembreServices MS=new MembreServices();
//static Date k =new Date(1995,9,5);
 // public static Membre z =new Membre(13,"Emna", "Debbabi","femme",k,"emna.debbabi@esprit.tn","Nabeul", 52978520,"emna123","emna123","Membre");
    @FXML
    private TextField Uaddnom;

    @FXML
    private TextField Uaddlieu;

    @FXML
    private DatePicker Udatepicker;

    @FXML
    private ComboBox<String> Ucombo_type;

    @FXML
    private ImageView Uverif_nom;

    @FXML
    private ImageView Uverif_type;

    @FXML
    private ImageView Uverif_date;

    @FXML
    private ImageView Uverif_lieu;

    @FXML
    private TextField Uadddescription;

    @FXML
    private ImageView Uverif_description;

    @FXML
    private TextField Uaddheure;

    @FXML
    private ImageView Uverif_heure;

    @FXML
    private ImageView UimageEvent;

    int c;
    int file = 0;
    File pDir;
    File pfile;
    String lien;

   

    @FXML
    private void showmyEvent(MouseEvent event) throws IOException {

        if (event.getClickCount() > 1) {
            int selectedIndex = listEvents.getSelectionModel().getSelectedIndex();
            if ((listEvents.getItems().get(selectedIndex)) instanceof Evenement) {

                ev = listEvents.getItems().get(selectedIndex);
                System.out.println(ev);
         Ucombo_type.getItems().addAll("Recyclage", "Ecologie", "Challenge", "Animaux");
        file = 0;
        c = (int) (Math.random() * (300000 - 2 + 1)) + 2;
        pDir = new File("src/img/addimage" + c + ".jpg");
        lien = "img/addimage" + c + ".jpg";
        Ucombo_type.setValue(ev.getType());
        Uaddnom.setText(ev.getNom());
        Uadddescription.setText(ev.getDescription());
        Uaddlieu.setText(ev.getLieu());
        LocalDate d = convertToLocalDateViaSqlDate(ev.getDate());
        Udatepicker.setValue(d);
        Uaddheure.setText(ev.getHeure());
        System.out.println(ev.getImg());
        Image image = new Image(ev.getImg());
        UimageEvent.setImage(image);
        System.out.println(ev.getImg());
//                Node source = (Node) event.getSource();
//                Stage stage = (Stage) source.getScene().getWindow();
//                //stage.close();
//                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("HomeEvent.fxml")));
//                stage.setScene(scene);

                //stage.show(); 
            }
        }
    }

    @FXML
    private void UAddImage(MouseEvent event) throws MalformedURLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select image..");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp")
        );
        Window stage = null;
        pfile = fileChooser.showOpenDialog(stage);

        /* - draw image */
        if (pfile != null) {
            file = 1;
            //ch.setText("image sélectionnée");
            Image image = new Image(pfile.toURI().toURL().toExternalForm());
            UimageEvent.setImage(image);
        }
    }

    public boolean copier(File source, File dest) {
        try (InputStream sourceFile = new java.io.FileInputStream(source);
                OutputStream destinationFile = new FileOutputStream(dest)) {
            // Lecture par segment de 0.5Mo  
            byte buffer[] = new byte[512 * 1024];
            int nbLecture;
            while ((nbLecture = sourceFile.read(buffer)) != -1) {
                destinationFile.write(buffer, 0, nbLecture);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false; // Erreur 
        }
        return true; // Résultat OK  
    }

    public Date convertToDateViaSqlDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }

    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }

    @FXML
    private void SaveEvent(ActionEvent event) throws IOException, SQLException, Exception {
        lien = "img/addimage" + c + ".jpg";
        //copier( pfile,pDir) ;
        Boolean test1, test2, test3, test4, test5, test6;
        test1 = test2 = test3 = test4 = test5 = test6 = true;
        if (Ucombo_type.getValue() == null) {
            System.out.println("pas de type");
            Image image1 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            Uverif_type.setImage(image1);
            test1 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            Uverif_type.setImage(image1);
        }
        if (Uaddnom.getText().matches("") || Uaddnom.getText().matches(".*\\d+.*")) {
            System.out.println("pas de nom");
            Image image2 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            Uverif_nom.setImage(image2);
            test2 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            Uverif_nom.setImage(image1);
        }
        if (Uadddescription.getText().matches("")) {
            System.out.println("pas de desc");
            Image image3 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            Uverif_description.setImage(image3);
            test3 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            Uverif_description.setImage(image1);
        }
        if (Uaddlieu.getText().matches("")) {
            System.out.println("pas de lieu");
            Image image4 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            Uverif_lieu.setImage(image4);
            test4 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            Uverif_lieu.setImage(image1);
        }
        if (Udatepicker.getValue() == null) {
            System.out.println("pas de date");
            Image image5 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            Uverif_date.setImage(image5);
            test5 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            Uverif_date.setImage(image1);
        }
        if (Uaddheure.getText().matches("")) {
            System.out.println("pas de lien fb");
            Image image5 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            Uverif_heure.setImage(image5);
            test5 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            Uverif_heure.setImage(image1);
        }
        if (test1 && test2 && test3 && test4 && test5 && test6) {
            if (file == 0) {
                LocalDate date = Udatepicker.getValue();
                Date d = convertToDateViaSqlDate(date);
                ev.setImg(lien);

                Evenement m = new Evenement(ev.getId(), Uaddnom.getText(), (String) Ucombo_type.getValue(), d, Uaddlieu.getText(),
                        Uadddescription.getText(), Uaddheure.getText(), ev.getImg(), ev.getIdM());

                System.out.println(ev.getId());
                ServiceEvent es = new ServiceEvent();
                
              //  es.updateEvenement(ev);
 es.MajEvent(m, ev.getId());
                //  System.out.println(ev.getImg());
                ev = m;

                infoBox("Successfull", "Success", null);
                
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                //stage.close();
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("HomeEvent.fxml")));
                stage.setScene(scene);
                //stage.show();
            } else {
                copier(pfile, pDir);
                ev.setImg(lien);
                LocalDate date = Udatepicker.getValue();
                Date d = convertToDateViaSqlDate(date);
                
                //  Evenement m = new Evenement(ev.getId_event(), ev.getId_user(), tf_nom.getText(), d, ev.getImg(), 
                //ta_description.getText(), tf_lieu.getText(), tf_lienFB.getText(), combo_cat.getValue());
                Evenement m = new Evenement(ev.getId(), Uaddnom.getText(), (String) Ucombo_type.getValue(), d, Uaddlieu.getText(),
                        Uadddescription.getText(), Uaddheure.getText(),ev.getImg(), ev.getIdM());
                //System.out.println(ev.getId_event());
                //   MajEvent(m, ev.getId_event());
                //   System.out.println(ev.getImg());
                
                ServiceEvent es = new ServiceEvent();
                //es.updateEvenement(ev);
                es.MajEvent(m, ev.getId());
                ev = m;
                infoBox("Successfull", "Success", null);
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                //stage.close();
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("HomeEvent.fxml")));
                stage.setScene(scene);
            }
        } else {
            infoBox("vérifiez le(s) champ(s) invalide(s)", "champ(s) invalide", null);
        }
    }

    public static void infoBox(String infoMessage, String titleBar, String headerMessage) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }
    
  
  
        @FXML
    private void deleteEvent(ActionEvent event) throws IOException {
      
         Evenement ev = listEvents.getSelectionModel().getSelectedItem();
          System.out.println(ev.getId());
        ServiceEvent es=new ServiceEvent();
        es.DelEvent(ev.getId());
        // infoBox("evenement bien supprimé", "Success", null);
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("HomeEvent.fxml")));
        stage.setScene(scene);
        stage.show();
    }
 @Override
    public void initialize(URL location, ResourceBundle resources) {
       try {
           System.out.println(MS.getidUSERByusername());
       } catch (SQLException ex) {
        
       }
 Lwelcome.setText("Bonjour "+session.getUser());
        try {

            ObservableList<Evenement> data = FXCollections.observableArrayList();

            String req = "SELECT * FROM `evttesting` where `idM`=?";
            PreparedStatement ste;

            ste = DB.getInstance().getConnection().prepareStatement(req);
            ste.setInt(1, MS.getidUSERByusername());
            ResultSet rs = ste.executeQuery();
            while (rs.next()) {
                data.add(new Evenement(rs.getInt("id"),rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"),rs.getInt("idM")));
             

            }

            listEvents.getItems().addAll(data);
            listEvents.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {
                public ListCell<Evenement> call(ListView<Evenement> arg0) {
                    return new ListCell<Evenement>() {
                        @Override
                        protected void updateItem(Evenement item, boolean bln) {
                            super.updateItem(item, bln);

                            if (item != null) {

                                VBox vBox = new VBox(
                                        new Text(item.getType()),
                                        new Text(item.getLieu()),
                                        new Text(item.getDescription()),
                                        new Text(item.getHeure())
                                );
                                vBox.setSpacing(4);

                            Image  image  = new Image(item.getImg(), true); 
                            System.out.println(item.getImg());
//
                            ImageView imv =new ImageView(image);
                            imv.setFitHeight(130);
                            imv.setFitWidth(130);
//                                Button supp = new Button("Supprimer");
//                            supp.addEventHandler(EventType.ROOT, eventHandler);
//                            VBox vbox2 = new VBox (new Button("Supprimer",onMouseClickedProperty(deleteEvent())); 
                                HBox hBox = new HBox(imv, new Text(item.getNom()),vBox);
                                hBox.setSpacing(10);
                                setGraphic(hBox);
 listEvents.setItems(data);
                            }
                        }
                    };
                }
            });
            // TODO
        } catch (SQLException ex) {

        }
 
    }
    /////////Menu
     @FXML
    void button1(ActionEvent event) throws IOException {
        
         //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ViewEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
          Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/HomeEvent.fxml")));
        stage.setScene(scene);
         
         
         

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AddEvent.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button4(ActionEvent event) throws IOException {
Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);
    }
    
    @FXML
    void backToMenu(ActionEvent event) throws IOException {
      Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Events.fxml")));
        stage.setScene(scene);

    }
         @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("Vous avez 3 évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
   //  notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }
}
