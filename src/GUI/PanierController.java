/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;

import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoClientException;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.SmsSubmissionResponseMessage;
import com.nexmo.client.sms.messages.TextMessage;

import database.DB;
import entities.Panier;

import entities.Membre;
import java.awt.Desktop;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import services.StockService;
import services.PanierService;

import java.util.Date;
import javafx.scene.control.TextField;


import java.util.Properties;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;




import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.controlsfx.control.Notifications;
import services.MembreServices;


/**
 * FXML Controller class
 *
 * @author brini
 */
public class PanierController implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
     private   TableView<Panier> tableview;
    
    
     @FXML
    private Button Btnvalider;

    @FXML
    private Button btnannulercmd;

     @FXML
    private Label lbtotalprix;
     
    
    @FXML
    private Button bntimprimer;
    
    
    
    @FXML
    private TextField tfModifier;

    @FXML
    private Button btnModif;
    
    @FXML
    private Label lbwarning;
        
    Connection cnx ;
     Statement statement ;
     ResultSet result ;
    
    
    
      @FXML
    private Button idcontinuer;
      
      MembreServices ms = new MembreServices();
              
//     
//    @FXML
//    private Button btnmodify;
     
     

    /**
     * Initializes the controller class.
   
     */
 
    
    @FXML
    public  void ImprimerFacture() throws FileNotFoundException, DocumentException, SQLException
    {
            PanierService cmd = new PanierService();
//            FactureService facture = new FactureService();
            cnx = DB.getInstance().getConnection();
         
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("facture.pdf"));
            document.open();
                    
                    
            /////////////////////TITLE//////////////////////////////////////////////
           Chunk glue = new Chunk(new VerticalPositionMark());
           Paragraph p = new Paragraph(new Phrase("SMART CYCLE",FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.BLACK)));
           p.add(new Chunk(glue));
           p.add(new Phrase("Facture",FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.GRAY)));
           p.setSpacingAfter(10);
           Paragraph p2 = new Paragraph(new Phrase("Adresse : Z.I. Chotrana II B.P. 160\n" +
           "Pôle Technologique El Ghazela  "));
           Paragraph p3 = new Paragraph(new Phrase("Ville : Ariana 2083"));
           document.add(p);
           document.add(p2);
           document.add(p3);
            ///// ////////////TABLE FACTURE DATE COIN DROITE DE PAGE//////////////
             PdfPTable table  = new PdfPTable(2);
             table.setHorizontalAlignment(Element.ALIGN_RIGHT);
             table.setWidthPercentage(160/5.23f);
             table.setSpacingAfter(5f);
             table.setSpacingBefore(0f); 
             float [] colwidth = {5f,5f};
             table.setWidths(colwidth);
             PdfPCell C1 = new PdfPCell(new Paragraph("Facture",FontFactory.getFont(FontFactory.TIMES_BOLD, 17, BaseColor.BLACK)));
             PdfPCell C2 = new PdfPCell(new Paragraph("Date",FontFactory.getFont(FontFactory.TIMES_BOLD, 17, BaseColor.BLACK)));
             PdfPCell C3 = new PdfPCell(new Paragraph(" ")); //NUMERO FAC
             
            // Instantiate a Date object
            Date date = new Date();
            // display time and date
            SimpleDateFormat ft =  new SimpleDateFormat (" yyyy.MM.dd ");            
            PdfPCell C4 = new PdfPCell(new Paragraph(ft.format(date))); //dATE
             C1.setBackgroundColor(BaseColor.GRAY);
             C2.setBackgroundColor(BaseColor.GRAY);
             C1.setBorderWidth(1f);
             C2.setBorderWidth(1f);
             C3.setBorderWidth(1f);
             C4.setBorderWidth(1f);
             table.addCell(C1);
             table.addCell(C2);
             table.addCell(C3);
             table.addCell(C4);  
             document.add(table);
             
             ////////////////////////////////Date limite de paiement/////////////////
              Paragraph pDate = new Paragraph(new Phrase ("Date Limite de paiement: ",FontFactory.getFont(FontFactory.TIMES_BOLD, 14, BaseColor.GRAY)));
              pDate.setAlignment(Element.ALIGN_RIGHT);
              pDate.setIndentationRight(10);
              pDate.setSpacingBefore(20f);
              pDate.setSpacingAfter(5f);
             
              SimpleDateFormat formattedDate = new SimpleDateFormat("yyyy.MM.dd");            
              Calendar c = Calendar.getInstance();        
              c.add(Calendar.DATE, 4);  // number of days to add      
              String datelimite = (String)(formattedDate.format(c.getTime()));
              Paragraph pDatelimite = new Paragraph(new Phrase (datelimite,FontFactory.getFont(FontFactory.TIMES_BOLD, 14, BaseColor.BLACK)));
              pDatelimite.setAlignment(Element.ALIGN_RIGHT);
              pDatelimite.setIndentationRight(100);
//              pDatelimite.setSpacingBefore(5f);
//              pDatelimite.setSpacingAfter(10f);
              
              document.add(pDate);
              document.add(pDatelimite);
                      
             ///////////////////////////////////////LES INFORMATIONS DE MEMBRE/////////////////////////////////////           
            PdfPTable table1 = new PdfPTable(1);
            table1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.setWidthPercentage(260/5.23f);
            table1.setSpacingBefore(10f); 
            PdfPCell cell = null;
            
            cell =    new PdfPCell(new Paragraph("Destinataire"));
            cell.setBackgroundColor(BaseColor.GRAY);
            cell.setMinimumHeight(20f);
            table1.addCell(cell);
            document.add(table1);    
            ///////////////////membre////////////////////////////////
            Membre m = new Membre();
            MembreServices ms1 = new MembreServices();
            m = ms1.getUSERByusername();
          
            Paragraph pNom = new Paragraph(new Phrase("Nom : "+m.getNom()));
            document.add(pNom);
            Paragraph pPrenom = new Paragraph(new Phrase("Prénom :  "+m.getPrenom()));
            document.add(pPrenom); 
            Paragraph pAdreese = new Paragraph(new Phrase("Adresse : "+m.getAdresse()));
            document.add(pAdreese); 
            Paragraph pNumTel = new Paragraph(new Phrase("NumTel :"+m.getTelephone()));
            document.add(pNumTel);
            Paragraph pMail = new Paragraph(new Phrase("E-mail :"+m.getEmail()));
            pMail.setSpacingAfter(40);   
            document.add(pMail);
            
            
            ///////////////////Table de Commande ////////////////////////
            
           PdfPTable table2 = new PdfPTable(3);
           float [] colwidth2 = {10f,5f,5f};
           table2.setWidths(colwidth2);
           table2.setWidthPercentage(100);
           PdfPCell cell2 = null;
      
          cell2 = new PdfPCell(new Phrase("Description",FontFactory.getFont(FontFactory.TIMES_BOLD, 14, BaseColor.BLACK)));
          cell2.setColspan(1);
          cell2.setMinimumHeight(20f);
          cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell2.setBackgroundColor(BaseColor.GRAY);
          table2.addCell(cell2);
          
          cell2 = new PdfPCell(new Phrase("Quantité",FontFactory.getFont(FontFactory.TIMES_BOLD, 14, BaseColor.BLACK)));
          cell2.setColspan(1);
          cell2.setBackgroundColor(BaseColor.GRAY);
          cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell2.setMinimumHeight(20f);
          table2.addCell(cell2);
          
          cell2 = new PdfPCell(new Phrase("Prix(DT) ",FontFactory.getFont(FontFactory.TIMES_BOLD, 14, BaseColor.BLACK)));
          
          cell2.setColspan(1);
          cell2.setBackgroundColor(BaseColor.GRAY);
          cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell2.setMinimumHeight(20f);
          table2.addCell(cell2);
          
         

          
         ///////////////////////////////Contenu de Table/////////////////////////////////////////////////
           int a = cmd.getALLCommandes(ms.getidUSERByusername()).size();
    
          for(int i = 0; i <a ; i++)
        {
        table2. addCell (getCell(tableview.getItems().get(i).getDescription(),PdfPCell.ALIGN_CENTER ));        
        table2.addCell(getCell(Double.toString(tableview.getItems().get(i).getQuantitedes()),PdfPCell.ALIGN_CENTER ));
  
        table2.addCell(getCell((Double.toString(tableview.getItems().get(i).getTotalprix())),PdfPCell.ALIGN_CENTER ));
        }
          
          cell2 = new PdfPCell(new Phrase("Montant total à payer ",FontFactory.getFont(FontFactory.TIMES_BOLD, 14, BaseColor.BLACK)));
          cell2.setColspan(2);
          cell2.setBackgroundColor(BaseColor.GRAY);
          cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell2.setMinimumHeight(20f);
          table2.addCell(cell2);
          
         String totalprix =String.valueOf(CalculTotalPrix());
         cell2 = new PdfPCell(new Phrase(totalprix));
         cell2.setColspan(1);
         cell2.setMinimumHeight(20f);
         cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
         table2.addCell(cell2);
          
         document.add(table2);
          
          ////////////////////////////////////////Close document/////////////////////////////////////////////////////////
          document.close();
          writer.close();
          
          ///////////////////////////////////////////////////////////////////////////////////////////////
        try
        {
            Desktop.getDesktop().open(new java.io.File("facture.pdf"));
        }
        catch (IOException ex) 
        {
            Logger.getLogger(PanierController.class.getName()).log(Level.SEVERE, null, ex);
        }
                   
    }
    ////////////////////////TotalPrixFacture///////////////////////////////////////
    
    private PdfPCell getCell(String text, int alignment)
    {
        PdfPCell cell = new PdfPCell(new Phrase(text));
        cell.setPadding(0);
       cell.setHorizontalAlignment(alignment);
       cell.setMinimumHeight(30f);
        return cell;
        
        
     }
    
    
    

     /////////////////////////////////Annuler Commande/////////////////////////////
    @FXML
    public void AnnulerCommande() throws SQLException
    {
         PanierService cmd = new PanierService();
         StockService service = new StockService();
         Panier x = tableview.getSelectionModel().getSelectedItem();
//         service.updateStockinc(x.getIdcat(), x.getQuantitedes());
         cmd.deleteCommande(x.getIdcat());
         
         Double qte = x.getQuantitedes();
         Double qtedispo = service.QtedispoCat(x.getIdcat());
         Double qtereste = qtedispo+qte ;
         
         service.updateStock(x.getIdcat(), qtereste);
         int id =cmd.IdCmd(ms.getidUSERByusername());
        cmd.UpdateTotal(x.getIdcmd(), CalculTotalPrix());
       
       
         tableview.setItems(cmd.getALLCommandes(ms.getidUSERByusername()));
         lbtotalprix.setText(String.valueOf(CalculTotalPrix()));
         
         
    }
    
    //////////////////////////////Calcul Prix Panier  /////////////////////////////////
    @FXML 
    public double CalculTotalPrix ()
    {
     double total = 0.0 ;
       
    for( Panier i : tableview.getItems())
    {
        total += i.getTotalprix();
        
    }
  
     return  total ;
        
    }
    ///////////////////////////ModifieCommade//////////////////////
    @FXML
    public void ModifierPanier() throws SQLException
    {
        StockService cat = new StockService();
        PanierService cmd = new PanierService();
        Panier c = tableview.getSelectionModel().getSelectedItem();
        //////////////////////////////////////////////////////
        double quantitedispo = cat.QtedispoCat(c.getIdcat());
        
        System.out.println(quantitedispo);
        double quantitemo=Double.parseDouble(tfModifier.getText());
        
        if (quantitedispo <quantitemo)
        { lbwarning.setVisible(true);
            tfModifier.clear();
            
        }
        else 
        {

        lbwarning.setVisible(false);
        c.setQuantitedes(quantitemo);
        //////////////////////////////////////////////////////
        double prixuni = cat.PrixuniCat(c.getIdcat());
        double prixnouv =prixuni*Double.parseDouble(tfModifier.getText());
        
         Double qte = c.getQuantitedes();
         Double qtedispo = quantitedispo+qte;
         Double qtereste = qtedispo-quantitemo;
         
        cat.updateStock(c.getIdcat(), qtereste);
        
        c.setTotalprix(prixnouv);
        int id =cmd.IdCmd(ms.getidUSERByusername());
        cmd.UpdateTotal(c.getIdcmd(), CalculTotalPrix());
        
        ////////////////////////////////////////////////////////////
      

        cmd.UpdateCommandePanier(c);
        
         
         tableview.setItems(cmd.getALLCommandes(ms.getidUSERByusername()));
         tableview.refresh();
         lbtotalprix.setText(String.valueOf(CalculTotalPrix()));
         tfModifier.setText("");
        }
    }
    ///////////////////////////Valider Commande/////////////////////
    @FXML
    public void ValiderCommande() throws IOException, AddressException, MessagingException, SQLException, NexmoClientException, ParseException
    {
         PanierService cmd = new PanierService();
         StockService service = new StockService();
         Panier x = tableview.getSelectionModel().getSelectedItem();
         
         for (Panier i : tableview.getItems())
         {
             service.updateStockdec(i.getIdcat(), i.getQuantitedes());
         }
         
         cmd.valideCommande(x.getIdcmd(), ms.getidUSERByusername());
        
       
         

         
         
 ///////////////////////////////Mail de confirmation de commande /////////////////////////////////////////////
 
         
 
          final String username = "briniskhawla2@gmail.com";
		final String password = "justbeyours @@";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator()
                  {
                              @Override
                      	protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
			}
                  } );
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("briniskhawla2@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse("briniskhawla2@gmail.com"));
			message.setSubject("Commande Effectué Avec succés  ");
			message.setText("Votre Commande a été bien enregistrée  Tous les détails se trouvent dans votre facture" );

			Transport.send(message);
//////////////////////////////////Notification Facture///////////////////////////////////////
			System.out.println("Done");
                        Image img = new Image("/icons/write.png");
                        Notifications notif = Notifications.create()
                                .title("Facture ")
                                .text("Veuillez Consultez Votre Facture ")
                                .graphic(new ImageView(img))
                                .hideAfter(Duration.seconds(10))
                                .position(Pos.TOP_RIGHT);
                        notif.show();
////////////////////////////////////////////Rappel/////////////////////////////                               
      Date date = new Date();
      
        SimpleDateFormat ft =  new SimpleDateFormat (" yyyy-MM-dd ");            
        String datedejour =ft.format(date);
    
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date2 = sdf.parse(datedejour);
       
        Date date1 = cmd.SelectedDate(ms.getidUSERByusername());
        
        
        
 
                      
       if (date1.compareTo(date2) > 0)    
       {
           
        cmd.deleteCommande(ms.getidUSERByusername());
    
           
           
           
            System.out.println("Date1 is after Date2");
        } else if (date1.compareTo(date2) < 0)
        {
            System.out.println("Date1 is before Date2");
        } else if (date1.compareTo(date2) == 0) 
        {
            
               
        
          NexmoClient client = new NexmoClient.Builder()
         .apiKey("a3dd8b6f")
         .apiSecret("LEAnZuYO4KhtaVkO")
         .build();

         String messageText = "RAPPEL DE PAIEMENT : Vous devez payer votre Facture de date  dans 2 jours ";
         TextMessage message2 = new TextMessage("Nexmo", "21620372189", messageText);

         SmsSubmissionResponse response = client.getSmsClient().submitMessage(message2);

        for (SmsSubmissionResponseMessage responseMessage : response.getMessages())
        {
         System.out.println(responseMessage);
         }
            
            
            
            
            
            
            
            
            
            System.out.println("Date1 is equal to Date2");
        } else
        {
            System.out.println("How to get here?");

    }
         
         
    }
    /////////////////////////SupprimerCommande
     @FXML 
    public void Supprimer () throws SQLException
    {
    PanierService cmd = new PanierService();
     StockService service = new StockService();
  
          int id =cmd.IdCmd(ms.getidUSERByusername());
          cmd.delete(id,ms.getidUSERByusername());
       
          
          for( Panier i : tableview.getItems())
    {
         Double qte =i.getQuantitedes();
         Double qtedispo = service.QtedispoCat(i.getIdcat());
         Double qtereste = qtedispo+qte ;
         
         service.updateStock(i.getIdcat(), qtereste);
        
    } 
          
    }
    
   
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
                lbwarning.setVisible(false);
        PanierService cmd = new PanierService();
        
        TableColumn DesColumn = new TableColumn("Description");
        TableColumn QteColumn = new TableColumn("Quantité");
        TableColumn PrixColumn = new TableColumn("Prix");
        TableColumn DateColumn = new TableColumn("Total");
            
        tableview.getColumns().addAll(DesColumn,QteColumn,PrixColumn,DateColumn);
        
        tableview.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        DesColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 ); 
        QteColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); 
        PrixColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 );
        DateColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 );
        
        
        DesColumn.setCellValueFactory(new PropertyValueFactory<Panier,String>("description"));    
        QteColumn.setCellValueFactory(new PropertyValueFactory<Panier,Double>("quantitedes"));
        PrixColumn.setCellValueFactory(new PropertyValueFactory<Panier,Double>("prix"));    
        DateColumn.setCellValueFactory(new PropertyValueFactory<Panier,Double>("totalprix"));
        try {
            tableview.setItems(cmd.getALLCommandes(ms.getidUSERByusername()));
        } catch (SQLException ex) {
            Logger.getLogger(PanierController.class.getName()).log(Level.SEVERE, null, ex);
        }
       lbtotalprix.setText(String.valueOf(CalculTotalPrix()));
      
    }    
    
 
    
  
    
}
