/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import entities.*;
import services.*;
import Utils.session;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author med
 */
public class User_associationController implements Initializable {

    @FXML
    TableView<Association> TVtable;
    @FXML
    private JFXListView<Association> LVlist;

    @FXML
    TextField TFscreen;
    @FXML
    TextField TFnom;
    @FXML
    TextField TFcapital;
    @FXML
    TextField TFrecherche;
    
    @FXML
    private BorderPane borderpane;

    @FXML
    private Label Lhello;

    @FXML
    private AnchorPane parent;

    
    @FXML
    private JFXComboBox<String> CBgovernorat;

    @FXML
    private JFXComboBox<String> CBvile;

    @FXML
    private TextField TFrue;
    
    @FXML
    private ImageView IVimage;
    
    MembreServices ms = new MembreServices();
    private FileChooser fileChooser ;
    private File file;
    private Image image;
    private FileInputStream fis;
    
    
    

    

    @FXML
    private Label Luser;
    
    Adresse a = new Adresse();
    
    
    
      /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) throws NumberFormatException{
        Lhello.setText("Bonjour "+session.getUser());
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        a.getGovBYid(1),
                        a.getGovBYid(2),
                        a.getGovBYid(3),
                        a.getGovBYid(4),
                        a.getGovBYid(5),
                        a.getGovBYid(6),
                        a.getGovBYid(7),
                        a.getGovBYid(8),
                        a.getGovBYid(9),
                        a.getGovBYid(10),
                        a.getGovBYid(11),
                        a.getGovBYid(12)
                );
        CBgovernorat.setItems(options);

        TableColumn nom = new TableColumn("Nom");
        TableColumn date = new TableColumn("DateCreation");
        TableColumn adresse = new TableColumn("Adresse");
        TableColumn c = new TableColumn("Capitale");
        TVtable.getColumns().addAll(nom, date,adresse, c);
        
        Associationservices as = new Associationservices();
        
//        Image image = as.get

        

        nom.setCellValueFactory(new PropertyValueFactory<Association, String>("nom_association"));
        date.setCellValueFactory(new PropertyValueFactory<Association, Date>("date_creation"));
        adresse.setCellValueFactory(new PropertyValueFactory<Association, String>("adresse"));
        c.setCellValueFactory(new PropertyValueFactory<Association, String>("capital"));

        try {
            TVtable.setItems(as.getAllUserOwnedAssociations(ms.getidUSERByusername()));
        } catch (SQLException ex) {
            Logger.getLogger(User_associationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        TFrecherche.textProperty().addListener((observable,oldvalue,newvalue)->{
            try {
                TVtable.setItems(as.SearshUserAssociations(TFrecherche.getText(),ms.getidUSERByusername()));
            } catch (SQLException ex) {
                Logger.getLogger(User_associationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
       
    }
    
    @FXML
    private void browse(ActionEvent event) throws Exception{
        
        fileChooser = new FileChooser();
        
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image files","*.png","*.jpg","*.gif"));
        Stage stagec = (Stage) parent.getScene().getWindow();
        file = fileChooser.showOpenDialog(stagec);
        if(file != null){
            //desktop.open(file);
            image = new Image(file.toURI().toString());
            IVimage.setImage(image);
            //TApath.setText(file.getAbsolutePath());
            
        }
    }
    
    @FXML
    void onAdressConfirmed(ActionEvent event) {
        int id = CBgovernorat.getSelectionModel().getSelectedIndex() + 1;
        System.out.println(id);
        CBvile.setItems(a.getVilleFroGov(id));

    }

    @FXML
    void button1(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/add_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        //current
    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/get_all_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button4(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
        stage.setScene(scene);
    }

    @FXML
    private void displayselected() throws Exception {
        Association asso = TVtable.getSelectionModel().getSelectedItem();
        Associationservices as = new Associationservices();
        TFnom.setText(asso.getNom_association());
        TFcapital.setText(String.valueOf(asso.getCapital()));
        System.out.println(asso.getId()); 
        IVimage.setImage(as.getAssociationImageByID(asso.getId()));
        //Associationservices as= new Associationservices();

    }
     @FXML
    private void deleteselected() throws Exception {
        Association asso = TVtable.getSelectionModel().getSelectedItem();
       // System.out.println(asso.getId()); 
        Associationservices as= new Associationservices();
        as.deleteAssociationById(asso.getId());
        TVtable.setItems(as.getAllUserOwnedAssociations(ms.getidUSERByusername()));

    }
    @FXML
    private void modifySelected() throws Exception {
        fis = new FileInputStream(file);
        Association asso = TVtable.getSelectionModel().getSelectedItem();
        
        a.setGov(CBgovernorat.getSelectionModel().getSelectedItem());
        a.setRue(TFrue.getText());
        a.setVille(CBvile.getSelectionModel().getSelectedItem());
        System.out.println(a.toString());
        String s = a.toString();
        
        asso.setNom_association(TFnom.getText());
        asso.setCapital(Double.parseDouble(TFcapital.getText()));
        asso.setAdresse(s);
        asso.setImage((InputStream)fis);
        
        System.out.println(asso.getId()); 
        Associationservices as= new Associationservices();
        as.updateAssociation(asso); // na9es l adresse
        TVtable.setItems(as.getAllUserOwnedAssociations(ms.getidUSERByusername())); //refresh
        TFnom.setText("");
        TFcapital.setText("");

    }
    
    @FXML
    void backToMenu() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) parent.getScene().getWindow();
        stagec.close();

    }


  

}
