package GUI;

import Utils.session;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import entities.Adresse;
import entities.Membre;
import Utils.ControleSaisie;
import java.io.IOException;
import services.MembreServices;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.StageStyle;

public class UserController implements Initializable {

    @FXML
    private BorderPane borderpane;
      @FXML
    private JFXTextField TFnom;

    @FXML
    private JFXTextField TFprenom;

    @FXML
    private JFXTextField TFemail;

    @FXML
    private JFXTextField TFntelephone;

    @FXML
    private JFXComboBox<String> CBgov;

    @FXML
    private JFXComboBox<String> CBville;

    @FXML
    private JFXTextField TFrue;

    @FXML
    private DatePicker DPdatenaissance;

    @FXML
    private RadioButton Rf;

    @FXML
    private RadioButton Rh;
    
    final private ToggleGroup tg = new ToggleGroup();


    @FXML
    private JFXPasswordField Tfmotdepasse;
    
    Adresse a = new Adresse();
    ControleSaisie cs = new ControleSaisie();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        a.getGovBYid(1),
                        a.getGovBYid(2),
                        a.getGovBYid(3),
                        a.getGovBYid(4),
                        a.getGovBYid(5),
                        a.getGovBYid(6),
                        a.getGovBYid(7),
                        a.getGovBYid(8),
                        a.getGovBYid(9),
                        a.getGovBYid(10),
                        a.getGovBYid(11),
                        a.getGovBYid(12)
                );
        CBgov.setItems(options);

        Rh.setToggleGroup(tg);

        Rf.setToggleGroup(tg);
        Rf.setSelected(true);


    }
     @FXML
    private void govChanged() {
        int id = CBgov.getSelectionModel().getSelectedIndex() + 1;
        System.out.println(id);
        CBville.setItems(a.getVilleFroGov(id));
    }
    
    @FXML
    private void onSinscrire() throws IOException {

        a.setGov(CBgov.getSelectionModel().getSelectedItem());
        a.setRue(TFrue.getText());
        a.setVille(CBville.getSelectionModel().getSelectedItem());
        String sex = "";
        if (Rf.isSelected()) {
            sex = Rf.getText();
        } else {
            sex = Rh.getText();
        }

        try {
            MembreServices ms = new MembreServices();

            if (!TFnom.getText().isEmpty() && !TFprenom.getText().isEmpty() && !TFemail.getText().isEmpty() && !TFntelephone.getText().isEmpty()
                    && !Tfmotdepasse.getText().isEmpty() && !TFrue.getText().isEmpty()) {
                if (cs.mailformat(TFemail.getText())) {
                    if (cs.GSM(TFntelephone.getText())) {
                       
                            LocalDate locald;
                            locald = DPdatenaissance.getValue();
                            Date date = Date.valueOf(locald);
                            Membre m = new Membre(TFnom.getText(), TFprenom.getText(), sex, date, TFemail.getText(), a.toString(),
                                    Integer.parseInt(TFntelephone.getText()),"", Tfmotdepasse.getText(), "user");
                            ms.updateUSER(m);
                            Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
                            Scene scene = new Scene(root, 1024, 768);
                            Stage stage = new Stage(StageStyle.DECORATED);
                            stage.setScene(scene);
                            stage.show();
                            Stage stagec = (Stage) TFnom.getScene().getWindow();
                            stagec.close();

                    } else {
                        Alert alert3 = new Alert(Alert.AlertType.ERROR);
                        alert3.setTitle("numero non valide!");
                        alert3.setHeaderText("Veuillez saisir numero de telephone valide !");
                        Optional<ButtonType> result = alert3.showAndWait();

                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("mail non valide!");
                    alert.setHeaderText("Veuillez saisir mail valide !");
                    Optional<ButtonType> result = alert.showAndWait();

                }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR);
                alert2.setTitle("Champs manquant!");
                alert2.setHeaderText("Veuillez saisir les champs requis !");
                Optional<ButtonType> result = alert2.showAndWait();
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

    }
    @FXML
    void backToMenu() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) TFnom.getScene().getWindow();
        stagec.close();

    }


}
