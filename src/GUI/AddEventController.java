/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import entities.Evenement;
import entities.Membre;
import Utils.session;

import com.itextpdf.text.Element;
//import static GUI.HomeEventController.z;

import services.ServiceEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.textfield.TextFields;
import services.MembreServices;

/**
 *
 * @author debba
 */
public class AddEventController implements Initializable {
     MembreServices MS=new MembreServices();
    int c;
    File pDir;
    File pfile;
    int file = 0;
    String lien;
    
    @FXML
    private TextField addnom;

    @FXML
    private TextField addlieu;

    @FXML
    private DatePicker datepicker;

    @FXML
    private ComboBox<String> combo_type;
    @FXML
    private ImageView verif_nom;

    @FXML
    private ImageView verif_type;

    @FXML
    private ImageView verif_date;

    @FXML
    private ImageView verif_lieu;
    @FXML
    private TextField adddescription;

    @FXML
    private ImageView verif_description;
    @FXML
    private TextField addheure;

    @FXML
    private ImageView verif_heure;
    @FXML
    private ImageView imageEvent;
     @FXML
    private Label Lwelcome;

    @FXML
    private void AddEvent(ActionEvent event) throws IOException, SQLException {

        //file = 0;
        Boolean test1, test2, test3, test4, test5, test6;
        test1 = test2 = test3 = test4 = test5 = test6 = true;
        if (combo_type.getValue() == null) {
            System.out.println("pas de type");
            Image image1 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            verif_type.setImage(image1);
            test1 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            verif_type.setImage(image1);
        }
        if (addnom.getText().matches("") || addnom.getText().matches(".*\\d+.*")) {
            System.out.println("pas de nom");
            Image image2 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            verif_nom.setImage(image2);
            test2 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            verif_nom.setImage(image1);
        }
        if (adddescription.getText().matches("")) {
            System.out.println("pas de description");
            Image image3 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            verif_description.setImage(image3);
            test3 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            verif_description.setImage(image1);
        }
        if (addlieu.getText().matches("")) {
            System.out.println("pas de lieu");
            Image image4 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            verif_lieu.setImage(image4);
            test4 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            verif_lieu.setImage(image1);
        }
     ///((datepicker.getValue() == null)&&((
        if (((!verifierDate(datepicker.getValue())))||(datepicker.getValue() == null)) {
          
            System.out.println(datepicker.getValue() );
            System.out.println("pas de date");
            Image image5 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            verif_date.setImage(image5);
            test5 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            verif_date.setImage(image1);
        }
        if (addheure.getText().matches("")) {
            System.out.println("pas d'heure");
            Image image5 = new Image("http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png");
            verif_heure.setImage(image5);
            test5 = false;
        } else {
            Image image1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Yes_Check_Circle.svg/120px-Yes_Check_Circle.svg.png");
            verif_heure.setImage(image1);
        }
        if (test1 && test2 && test3 && test4 && test5 && test6) {
            if (file == 0) {
                File m = new File("src/img/addimage.jpg");
                copier(m, pDir);

                ServiceEvent evenement = new ServiceEvent();
                int Min = 100000;
                int Max = 1000000;

                int x = Min + (int) (Math.random() * ((Max - Min) + 1));
                //System.out.println(tfnombre.getText());
                LocalDate date = datepicker.getValue();
                Date d = convertToDateViaSqlDate(date);
                System.out.println(MS.getidUSERByusername());
//                System.out.println(d);
                //   Event p = new Event(z.getId(), addnom.getText(), d, lien, ta_description.getText(),
                //           tf_lienFB.getText(), tf_lieu.getText(), combo_cat.getValue());
                Evenement p = new Evenement(addnom.getText(), (String) combo_type.getValue(), d, addlieu.getText(),
                         adddescription.getText(), addheure.getText(),lien,MS.getidUSERByusername());

                System.out.println(combo_type.getValue());
                System.out.println(d);
                /**
                 * **********
                 */
                evenement.createEvenement(p);

                infoBox("Successfull ", "Success", null);
//                Node source = (Node) event.getSource();
//                Stage stage = (Stage) source.getScene().getWindow();
                //stage.close();
                //  Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLEvents.fxml")));
                //  stage.setScene(scene);
                //stage.show();                
            } else {
                copier(pfile, pDir);

                ServiceEvent evenement = new ServiceEvent();
                int Min = 100000;
                int Max = 1000000;

                int x = Min + (int) (Math.random() * ((Max - Min) + 1));
                //System.out.println(tfnombre.getText());
                LocalDate date = datepicker.getValue();
                Date d = convertToDateViaSqlDate(date);
//                Event p = new Event(z.getId(), tf_nom.getText(), d, lien, ta_description.getText(),
//                        tf_lienFB.getText(), tf_lieu.getText(), combo_cat.getValue());
               Evenement p = new Evenement(addnom.getText(), (String) combo_type.getValue(), d, addlieu.getText(),
                         adddescription.getText(), addheure.getText(),lien,MS.getidUSERByusername());
                System.out.println(combo_type.getValue());
                System.out.println(d);
                /**
                 * **********
                 */
                evenement.createEvenement(p);
        //       list.refresh();
 //list.getListView().getItems().remove(getItem());
                infoBox("Successfull", "Success", null);
//                    Node source = (Node) event.getSource();
//                Stage stage = (Stage) source.getScene().getWindow();
//               stage.close();
//                     Scene scene = new Scene(FXMLLoader.load(getClass().getResource("EventHome.fxml")));
//                      stage.setScene(scene);
//                stage.show(); 

            }
        } else {
            infoBox("vérifiez le(s) champ(s) invalide(s)", "champ(s) invalide", null);
        }
      
    }
public static boolean verifierDate(LocalDate d1){
LocalDate d2 = LocalDate.now();
       
        if(d1.isBefore(d2)){return false;}
        else{    return true;}
}
    public static boolean copier(File source, File dest) {
        try (InputStream sourceFile = new java.io.FileInputStream(source);
                OutputStream destinationFile = new FileOutputStream(dest)) {
            // Lecture par segment de 0.5Mo  
            byte buffer[] = new byte[512 * 1024];
            int nbLecture;
            while ((nbLecture = sourceFile.read(buffer)) != -1) {
                destinationFile.write(buffer, 0, nbLecture);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false; // Erreur 
        }
        return true; // Résultat OK   
    }

    public  Date convertToDateViaSqlDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }

    public static void infoBox(String infoMessage, String titleBar, String headerMessage) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }

    @FXML
    private void AddImage(MouseEvent event) throws MalformedURLException {
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select image..");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp")
        );
        Window stage = null;
        pfile = fileChooser.showOpenDialog(stage);

        /* - draw image */
        if (pfile != null) {
            file = 1;
           Image image = new Image(pfile.toURI().toURL().toExternalForm());
            imageEvent.setImage(image);
        }
    }
     @FXML
    private void CancelEvent(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("AddEvent.fxml")));
        stage.setScene(scene);
    }
  
       @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        Lwelcome.setText("Bonjour "+session.getUser());
              file = 0;
        String[] les_villes = {"Ariana", "Béja", "Ben Arous", "Bizerte", "Gabès",
            "Gafsa", "Jendouba", "Kairouan", "Kasserine", "Kébili", "Kef", "Mahdia",
            "Manouba", "Médenine", "Monastir", "Nabeul", "Sfax", "Sidi Bouzid",
            "Siliana", "Sousse", "Tataouine", "Tozeur", "Tunis", "Zaghouan"};
        TextFields.bindAutoCompletion(addlieu, les_villes);
        combo_type.getItems().addAll("Recyclage", "Ecologie", "Challenge", "Animaux");
        c = (int) (Math.random() * (300000 - 2 + 1)) + 2;
        pDir = new File("src/img/addimage" + c + ".jpg");
        lien = "img/addimage" + c + ".jpg";
        // TODO
    }
    ////////////////Menu
@FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ViewEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/HomeEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AddEvent.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button4(ActionEvent event) throws IOException {
Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Events.fxml")));
        stage.setScene(scene);

    }
         @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("Vous avez 3 évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
   //  notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }
}
