/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import database.DB;
import entities.Commande;

import entities.Panier;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import services.StockService;
import services.MembreServices;
import services.PanierService;

/**
 * FXML Controller class
 *
 * @author brini
 */
public class CategorieController implements Initializable 
{    
    Connection conn ;
    PreparedStatement pst ;
    ResultSet rs  ;
    Statement stm ;
    

    @FXML
    private HBox hbox;
    @FXML
    private VBox vbox;
    
    @FXML
    private AnchorPane pane;
    @FXML
    private ListView<String> listview ;
    
       @FXML
    private Button btnajout;

    @FXML
    private TextField tfqtdeserie;
    
    @FXML
    private ImageView imgpanier;
    
    @FXML
    private TextField tfqtedispo;

    @FXML
    private TextField tfprixuni;
    @FXML
    private TextField tfidcat;
    
    @FXML
    private Label lbajoutcmd;
    
    @FXML
    private Label lbwarning;
    
    @FXML
    private TextField recherchecat;
    
    MembreServices ms = new MembreServices();
  
   

    @FXML
    private Label lbprix;
    private final Image IMAGE_Acier  = new Image("/images/a2.jpg");
    private final Image IMAGE_Plastique  = new Image("/images/a4.jpg");
    private final Image IMAGE_Verre  = new Image("/images/a1.jpg");
    private final Image IMAGE_Carton = new Image("/images/a3.jpg");
      private final Image IMAGE_Elec = new Image("/images/a6.jpg");
    
    

    private Image[] listOfImages = {IMAGE_Acier, IMAGE_Plastique, IMAGE_Verre,IMAGE_Carton,IMAGE_Elec};

    /**
     * Initializes the controller class.
     */
    
    
    @FXML
    void Calculerprix(ActionEvent event) 
    {
        
        
       double d1= Double.parseDouble(tfqtdeserie.getText());
       double d2 = Double.parseDouble(tfprixuni.getText());
       double d3 = Double.parseDouble(tfqtedispo.getText());
       
       if (d1 > d3 )
       {
           lbwarning.setVisible(true);
           tfqtdeserie.clear();
           lbprix.setText("");
           
       }
       else 
       {
       double d4=d1*d2;
       
        lbwarning.setVisible(false);
        lbprix.setText(String.valueOf(d4));
       }
    }
    
    /////TEXTFIELD/////////////////////
 
 
 
    @FXML
    void ajoutpanier(ActionEvent event) throws SQLException 
    {
       
      PanierService cmd = new PanierService();
      StockService cat = new StockService();
             
        if(tfqtdeserie.getText().equals(""))
        {
                                Alert alertu = new Alert(Alert.AlertType.ERROR);
				alertu.setTitle("pas de choix!");
				alertu.setHeaderText("vous devez entrez la quantité désirée !");
				Optional<ButtonType> result = alertu.showAndWait();
        }    
        else if (   ! cmd.ExistCommande( ms.getidUSERByusername() ))
        {
                String etat ="0";
                Commande c = new Commande(ms.getidUSERByusername(),etat);
              int id =  cmd.createCommande(c);
                Double prix = cat.PrixuniCat(Integer.parseInt(tfidcat.getText()));
             
                System.out.println(id);
            if (   cmd.ExistCategorie(Integer.parseInt(tfidcat.getText()),id)  )     
            {
                
           
                Double qte= cmd.Qte(Integer.parseInt(tfidcat.getText()), ms.getidUSERByusername());
                Double qtedes= qte+ Double.parseDouble(tfqtdeserie.getText());
//                Double qtedispo =
                Double tot = cmd.total(id,ms.getidUSERByusername());
                Double total = tot+Double.parseDouble(lbprix.getText());
               
                
                
                Double qtedispo = cat.QtedispoCat(Integer.parseInt(tfidcat.getText()));
                Double qtereste = qtedispo- Double.parseDouble(tfqtdeserie.getText());
                cat.updateStock(Integer.parseInt(tfidcat.getText()), qtereste) ;
                
               cmd.UpdateCommande(Integer.parseInt(tfidcat.getText()), qtedes,prix,total);
               cmd.UpdateTotal(id, total);
               
               lbajoutcmd.setVisible(true);
               lbwarning.setVisible(false); 
           
            }
           
           else if (  !cmd.ExistCategorie(Integer.parseInt(tfidcat.getText()),id) )
           
           {
           Panier c1 = new Panier( id,Integer.parseInt(tfidcat.getText()),Double.parseDouble(tfqtdeserie.getText()),prix,
           Double.parseDouble(lbprix.getText()));
                Double tot = cmd.total(id,ms.getidUSERByusername());
                Double total = tot+Double.parseDouble(lbprix.getText());
                
                
                Double qtedes=  Double.parseDouble(tfqtdeserie.getText());
                Double qtedispo = cat.QtedispoCat(Integer.parseInt(tfidcat.getText()));
               
                
                Double qtereste = qtedispo-qtedes;
                cat.updateStock(Integer.parseInt(tfidcat.getText()), qtereste) ;
             cmd.UpdateTotal(id, total);
            cmd.create(c1);
            lbajoutcmd.setVisible(true);
            lbwarning.setVisible(false); 
           }
            
                 
                 
        }
      
      
        else if(   cmd.ExistCommande( ms.getidUSERByusername()   ))
        {
            int id = cmd.IdCmd( ms.getidUSERByusername() );
             Double prix = cat.PrixuniCat(Integer.parseInt(tfidcat.getText()));
            if (   cmd.ExistCategorie(Integer.parseInt(tfidcat.getText()),id)  )
               
            {
                  Double tot = cmd.total(id,ms.getidUSERByusername());
                Double total = tot+Double.parseDouble(lbprix.getText());
          
                Double qte= cmd.Qte(Integer.parseInt(tfidcat.getText()), ms.getidUSERByusername());
                Double qtedes= qte+ Double.parseDouble(tfqtdeserie.getText());
                        
                Double qtedispo = cat.QtedispoCat(Integer.parseInt(tfidcat.getText()));
                Double qtereste = qtedispo- Double.parseDouble(tfqtdeserie.getText());
                cat.updateStock(Integer.parseInt(tfidcat.getText()), qtereste) ;
                
               cmd.UpdateCommande(Integer.parseInt(tfidcat.getText()), qtedes,prix,total);
                 cmd.UpdateTotal(id, total);
                lbajoutcmd.setVisible(true);
               lbwarning.setVisible(false); 
           
            }
           
           else if (  !cmd.ExistCategorie(Integer.parseInt(tfidcat.getText()),id) )
           
           {
            Panier c2 = new Panier( id,Integer.parseInt(tfidcat.getText()),Double.parseDouble(tfqtdeserie.getText()),prix,
            Double.parseDouble(lbprix.getText()));
      
            cmd.create(c2);
             Double tot = cmd.total(id,ms.getidUSERByusername());
                Double total = tot+Double.parseDouble(lbprix.getText());
                    Double qtedes=  Double.parseDouble(tfqtdeserie.getText());
                Double qtedispo = cat.QtedispoCat(Integer.parseInt(tfidcat.getText()));
                 Double qtereste = qtedispo-qtedes;
                cat.updateStock(Integer.parseInt(tfidcat.getText()), qtereste) ;
                
             cmd.UpdateTotal(id, total);
            lbajoutcmd.setVisible(true);
            lbwarning.setVisible(false); 
           }
            
            
        }
    
    
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {  
          
       tfidcat.setVisible(false); //false
       lbajoutcmd.setVisible(false);
       lbprix.setText(null);
       lbwarning.setVisible(false);
        
        StockService cmd = new StockService();
        PanierService servicecmd = new PanierService();
//   
        final ObservableList<String> list2 = FXCollections.observableArrayList(cmd.descCategorie());
//        
        listview.setItems(list2);
        

        
            listview.setCellFactory(param -> new ListCell<String>() {
            private ImageView imageView = new ImageView();
            @Override
            public void updateItem(String name, boolean empty) {
                super.updateItem(name, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    if(name.equals("Metal"))
                        imageView.setImage(listOfImages[0]);
                    else if(name.equals("Plastique"))
                        imageView.setImage(listOfImages[1]);
                    else if(name.equals("Verre"))
                        imageView.setImage(listOfImages[2]);
                    else if(name.equals("Papier"))
                        imageView.setImage(listOfImages[3]);
                      else if(name.equals("Electronique"))
                        imageView.setImage(listOfImages[4]);
                   
                    setText(name);
                    setGraphic(imageView);
                }
            }
        });
            
          
                
        listview.setOnMouseClicked(e ->
        {
         lbajoutcmd.setVisible(false);
         tfqtdeserie.clear();
         lbprix.setText("");
         lbwarning.setVisible(false);
         
        try 
        {
            String req = "Select id_cat ,quantitedispo ,prixuni  From  stock where description = ? ";
            
            pst =  DB.getInstance().getConnection().prepareStatement(req);
            pst.setString(1 , (String)listview.getSelectionModel().getSelectedItem());
            rs= pst.executeQuery(); 
            
            while(rs.next())
            {   
                 tfidcat.setText(String.valueOf(rs.getInt("id_cat")));
                tfqtedispo.setText(String.valueOf(rs.getDouble("quantitedispo")));
                tfprixuni.setText(String.valueOf(rs.getDouble("prixuni")));
               
            }
             
            pst.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CategorieController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
             double d = Double.parseDouble(tfqtedispo.getText());
             if (d == 0)
             {
                   Alert alertu = new Alert(Alert.AlertType.ERROR);
					alertu.setTitle("Fin de Stock ");
					alertu.setHeaderText("Ce produit n’est plus disponible à la vente");
					Optional<ButtonType> result = alertu.showAndWait();
                              
             }
   });
        
    recherchecat.textProperty().addListener((Obse,oldval,newval)->{
        StockService cs = new StockService();
        
           try {
               //
               listview.setItems(cs.descSearchCategorie(recherchecat.getText()));
           } catch (SQLException ex) {
               Logger.getLogger(CategorieController.class.getName()).log(Level.SEVERE, null, ex);
           }
       
    });
        

    }
        
        
        
  
      
    
}
