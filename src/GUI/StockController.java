/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import entities.Catégorie;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import services.StockService;

/**
 * FXML Controller class
 *
 * @author brini
 */
public class StockController implements Initializable {

   
      @FXML
    private TableView<Catégorie> idtable;
        @FXML
    private TextField tfdesc;

    @FXML
    private TextField tfprix;

    @FXML
    private TextField tfqte;  
      
       @FXML
    private Button btnajout;

    @FXML
    private Button btnmodif; 
    
     @FXML
    private Label idmodif;
         @FXML
    private JFXButton btnsupp;
     
       StockService catservice = new StockService(); 
      
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {   
        idmodif.setVisible(false);
        StockService cm = new  StockService();
        TableColumn DesColumn = new TableColumn("Description");
        TableColumn QteColumn = new TableColumn("QuantitéDisponible");
        TableColumn PrixColumn = new TableColumn("Prix Unitaire ");
    
            
        idtable.getColumns().addAll(DesColumn,QteColumn,PrixColumn);
        
        idtable.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        DesColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 40 ); 
        QteColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 ); 
        PrixColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 30 );
    
        
        
        DesColumn.setCellValueFactory(new PropertyValueFactory<Catégorie,String>("description"));    
        QteColumn.setCellValueFactory(new PropertyValueFactory<Catégorie,Double>("quantitedispo"));
        PrixColumn.setCellValueFactory(new PropertyValueFactory<Catégorie,Double>("prixuni"));    
       
        idtable.setItems(cm.getALLCatégorie());
      
    }    
        @FXML
    void ajoutDeStock(ActionEvent event) 
    {
     
      Catégorie c = new Catégorie();
      c.setDescription(tfdesc.getText());
      c.setQuantitedispo(Double.parseDouble(tfqte.getText()));
      c.setPrixuni(Double.parseDouble(tfprix.getText()));
        System.out.println(tfdesc.getText());
      
       catservice.create(c);
       idtable.refresh();
       idtable.setItems(catservice.getALLCatégorie());
       tfdesc.setText("");
       tfqte.setText("");
       tfprix.setText(""); 
        
        
        
    }
    
    
        @FXML
    void suppstock(ActionEvent event)
    
    {
       Catégorie c = idtable.getSelectionModel().getSelectedItem();
       catservice.delete(c);
        idtable.refresh();
       idtable.setItems(catservice.getALLCatégorie());
    }
    
    

    @FXML
    void modificationdestock(ActionEvent event) 
    {
      Catégorie c = idtable.getSelectionModel().getSelectedItem();
//      c.setDescription(tfdesc.getText());
      c.setQuantitedispo(Double.parseDouble(tfqte.getText()));
      c.setPrixuni(Double.parseDouble(tfprix.getText()));
      
      catservice.UpdateSotck(c);
      idtable.refresh();
      idtable.setItems(catservice.getALLCatégorie());
      tfdesc.setText("");
      tfqte.setText("");
      tfprix.setText("");
      idmodif.setVisible(true);
    }
    
      
  
           
    
        
    
    
    
    
    
}
