package GUI;

import database.DB;
import static database.DB.getInstance;
import Utils.session;
import services.*;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class Admin_associationStatsController implements Initializable {

    @FXML
    private BorderPane borderpane;

    @FXML
    private AnchorPane parent;

    @FXML
    private Label Lhello;

    @FXML
    private TextField TFidea;

    @FXML
    private Label Lna;

    @FXML
    private Label La;

    @FXML
    private BarChart<?, ?> Bchart;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;

    @FXML
    private Pane Pchart;

    private Stage stage;

    MembreServices ms = new MembreServices();
    Associationservices as = new Associationservices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {

            Lhello.setText("Bonjour " + session.getUser());
            int nbr_adh = as.countAdherent();
            int nbr_mbr = ms.countMembres();
            La.setText(String.valueOf((nbr_adh * 100) / nbr_mbr) + "%");
            Lna.setText(String.valueOf(((nbr_mbr - nbr_adh) * 100) / nbr_mbr) + "%");

            ObservableList<PieChart.Data> list = FXCollections.observableArrayList();
            list.add(new PieChart.Data("Membre adhérer", nbr_adh));
            list.add(new PieChart.Data("Membre non adhérer", nbr_mbr - nbr_adh));
            //list.add(new PieChart.Data("test",8));
            PieChart pie = new PieChart(list);
            //pieRadius = Math.min(contentWidth,contentHeight) / 2;

            Pchart.getChildren().add(pie);
        } catch (Exception e) {
        }
        try {
            ObservableList<Integer> list1 = FXCollections.observableArrayList();
            ObservableList<String> list2 = FXCollections.observableArrayList();
            Connection connection;
            DB db = getInstance();
            connection = db.getConnection();

            PreparedStatement ps = connection.prepareStatement("SELECT COUNT(a.idM) ,ass.nom_association FROM demande a INNER JOIN association ass ON ass.id_association =a.idA GROUP by ass.id_association");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Associationservices k = new Associationservices();
                list1.add(rs.getInt(1));
                list2.add(rs.getString(2));
            }
            System.out.println("iiiiiii");
            System.out.println(list1);
            System.out.println(list2);
            XYChart.Series set1 = new XYChart.Series<>();

            for (int k = 0, p = 0; k < list2.size() && p < list1.size(); k++, p++) {

                System.out.println(k);
                set1.getData().add(new XYChart.Data(list2.get(k), list1.get(p)));
            }
            Bchart.getData().addAll(set1);

        } catch (SQLException ex) {
        }

    }

    @FXML
    private void showpie() {
        Pchart.setVisible(true);
        Bchart.setVisible(false);

    }

    @FXML
    private void showdiag() {
        Pchart.setVisible(false);
        Bchart.setVisible(true);

    }

    @FXML
    private void publish() throws SQLException {

    }

    @FXML
    void button1(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/admin_associationFeedbacks.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        //current

    }

//    @FXML
//    void button3(ActionEvent event) throws IOException {
//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
//LocalDate d;
//LocalDate locald;
//                            //locald = DPdatenaissance.getValue();
//                             date = java.sql.Date.valueOf(locald);
//
//    }
//
//    @FXML
//    void button4(ActionEvent event) throws IOException {
//        Node source = (Node) event.getSource();
//        Stage stage = (Stage) source.getScene().getWindow();
//        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
//        stage.setScene(scene);
//    }
    @FXML
    void backToMenu() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/MenuAdmin.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) parent.getScene().getWindow();
        stagec.close();

    }

}
