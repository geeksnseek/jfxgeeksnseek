/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import database.DB;
import entities.Evenement;
import entities.Membre;
import Utils.session;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import services.ParticipantService;
import services.ServiceEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import services.MembreServices;
/**
 *
 * @author debba
 */
public class ViewEventController implements Initializable {
@FXML
    private Label Lwelcome;
    @FXML
    private BorderPane borderpane;
    static String valeur_combo_events;
    static String valeur_combo_date;
    public static Evenement ev;
    @FXML
    private ComboBox<String> combo_catgorie;
    @FXML
    private ComboBox<String> combo_date;
    @FXML
    private ListView<Evenement> list;
    @FXML
    private ImageView imv;
    ObservableList<Evenement> data = FXCollections.observableArrayList();
    MembreServices MS= new MembreServices();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lwelcome.setText("Bonjour "+session.getUser());
        combo_catgorie.getItems().addAll("Recyclage", "Ecologie", "Challenge", "Animaux");
        

        combo_date.getItems().addAll("La semaine suivante", "La semaine précédente");
        if (valeur_combo_events == null && valeur_combo_date == null) {
            try {
                String req = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM `evttesting`";
                PreparedStatement ste;

                ste = DB.getInstance().getConnection().prepareStatement(req);
                ResultSet rs = ste.executeQuery();

                while (rs.next()) {
                   
               
                    //data.add(new Covoiturage(rs.getInt("id_cov"), rs.getInt("id_user"),rs.getDate("date_depart"),rs.getInt("heure_depart"), rs.getInt("minute_depart"),rs.getString("ville_depart"), rs.getString("adr_depart"), rs.getString("ville_arrive"),rs.getString("adr_arrive"),rs.getInt("nbr_places"),rs.getInt("cotisation"),rs.getString("description"),rs.getInt("nbr_reserve")));
                    data.add(new Evenement(rs.getInt("id"), rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"), rs.getInt("idM")));
                 
                }
                list.getItems().addAll(data);
                System.out.println(list);

                list.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {
                    @Override
                    public ListCell<Evenement> call(ListView<Evenement> param) {
                        return new ListCell<Evenement>() {
                            @Override
                            protected void updateItem(Evenement item, boolean bln) {
                                super.updateItem(item, bln);
                                if (item != null) {

                                    VBox vBox = new VBox(
                                            new Text(item.getNom()),
                                            new Text(item.getType()),
                                            new Text(item.getLieu()),
                                            new Text(item.getDescription()),
                                            new Text(item.getHeure())
                                    );
                                    vBox.setSpacing(4);

                                    Image image = new Image(String.valueOf(item.getImg()), true);
                                    ImageView imv = new ImageView(image);
                                    imv.setFitHeight(130);
                                    imv.setFitWidth(130);

                                    VBox vBox2 = new VBox();
                                    Button ignorer = new Button("Ingnorer");
                                    ignorer.setOnAction((e) -> getListView().getItems().remove(getItem()));
                                    vBox2.getChildren().add(ignorer);
                                   Button btn = new Button();
        btn.setText("Participer");
        btn.setOnAction((ActionEvent event) -> {
                             try {
                               ParticipantService es= new ParticipantService();
                                 if(es.getParticipantByIdevent(item.getId())) 
                                {     
                                 es.createParticipant(item);
                               getListView().getItems().remove(getItem());}
                                 else{
                                    infoBox("Vous avez déjà participé", "Existe déjà", null);
                                 }
                            
//                             
//                                 try {
//                                           Node source = (Node) event.getSource();
//                                 Stage stage = (Stage) source.getScene().getWindow();
//                                 Scene scene;
//                                     scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
//                                     stage.setScene(scene);
//                                 } catch (IOException ex) {
//                                     Logger.getLogger(ViewEventController.class.getName()).log(Level.SEVERE, null, ex);
//                                 }
                                     
//                              list.refresh();
//                               
//                                 list.getItems().clear();
                                
                              
                               
                                 
                             } catch (SQLException ex) {
                                 
                             }
            
});
          list.setItems(data);  
         VBox vBox3 = new VBox();
                                   
                                    vBox3.getChildren().add(btn);
                                    HBox hBox = new HBox(imv,vBox, new Text(String.valueOf(item.getDate())),vBox2,vBox3);
                                    hBox.setSpacing(50);

                                    setGraphic(hBox);

                                }
                            }

                        };
                    }

                });

                valeur_combo_date = null;
                valeur_combo_date = null;
            } catch (SQLException ex) {
                //
            }
        } else if (valeur_combo_events == null && valeur_combo_date == "La semaine suivante") {

            try {
                String req = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM `evttesting` WHERE DATEDIFF(date,NOW())> 0 and DATEDIFF(date,NOW())< 8";
                PreparedStatement ste;

                ste = DB.getInstance().getConnection().prepareStatement(req);
                ResultSet rs = ste.executeQuery();

                while (rs.next()) {

                    //data.add(new Covoiturage(rs.getInt("id_cov"), rs.getInt("id_user"),rs.getDate("date_depart"),rs.getInt("heure_depart"), rs.getInt("minute_depart"),rs.getString("ville_depart"), rs.getString("adr_depart"), rs.getString("ville_arrive"),rs.getString("adr_arrive"),rs.getInt("nbr_places"),rs.getInt("cotisation"),rs.getString("description"),rs.getInt("nbr_reserve")));
                    data.add(new Evenement(rs.getInt("id"), rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"), rs.getInt("idM")));

                }
                list.getItems().addAll(data);

                list.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {

                    @Override
                    public ListCell<Evenement> call(ListView<Evenement> arg0) {
                        return new ListCell<Evenement>() {

                            @Override
                            protected void updateItem(Evenement item, boolean bln) {
                                super.updateItem(item, bln);
                                if (item != null) {

                                    VBox vBox = new VBox(
                                            new Text(item.getNom()),
                                            new Text(item.getType()),
                                            new Text(item.getLieu()),
                                            new Text(item.getDescription()),
                                            new Text(item.getHeure())
                                    );
                                    vBox.setSpacing(4);

                                    Image image = new Image(String.valueOf(item.getImg()), true);
                                    ImageView imv = new ImageView(image);
                                    imv.setFitHeight(130);
                                    imv.setFitWidth(130);
                                    VBox vBox2 = new VBox();
                                    Button ignorer = new Button("Ingnorer");
                                    ignorer.setOnAction((e) -> getListView().getItems().remove(getItem()));
                                    vBox2.getChildren().add(ignorer);
                                        Button btn = new Button();
        btn.setText("Participer");
        btn.setOnAction((ActionEvent event) -> {
                             try {
                                 ParticipantService es= new ParticipantService();
                                   
                                 es.createParticipant(item);
                                 getListView().getItems().remove(getItem());
//                              list.refresh();
//                               
//                                 list.getItems().clear();
                                
//                                 Node source = (Node) event.getSource();
//                                 Stage stage = (Stage) source.getScene().getWindow();
//                                 Scene scene;
//                                   scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
//                                     stage.setScene(scene);
                               
                                 
                             } catch (SQLException ex) {
                                
                             }
            
});
         list.setItems(data);  
         VBox vBox3 = new VBox();
                                   
                                    vBox3.getChildren().add(btn);
                                    HBox hBox = new HBox(imv, vBox, new Text(String.valueOf(item.getDate())), vBox2,vBox3);
                                    hBox.setSpacing(10);

                                    setGraphic(hBox);

                                }
                            }

                        };
                    }

                });
                valeur_combo_date = null;
                valeur_combo_date = null;
            } catch (SQLException ex) {

            }
        } else if (valeur_combo_events != null && valeur_combo_date == null) {
            try {
                String req = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM `evttesting` WHERE type=" + "'" + valeur_combo_events + "'";
                PreparedStatement ste;

                ste = DB.getInstance().getConnection().prepareStatement(req);
                ResultSet rs = ste.executeQuery();

                while (rs.next()) {

                    //data.add(new Covoiturage(rs.getInt("id_cov"), rs.getInt("id_user"),rs.getDate("date_depart"),rs.getInt("heure_depart"), rs.getInt("minute_depart"),rs.getString("ville_depart"), rs.getString("adr_depart"), rs.getString("ville_arrive"),rs.getString("adr_arrive"),rs.getInt("nbr_places"),rs.getInt("cotisation"),rs.getString("description"),rs.getInt("nbr_reserve")));
                    data.add(new Evenement(rs.getInt("id"), rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"), rs.getInt("idM")));

                }
                list.getItems().addAll(data);

                list.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {

                    @Override
                    public ListCell<Evenement> call(ListView<Evenement> arg0) {
                        return new ListCell<Evenement>() {

                            @Override
                            protected void updateItem(Evenement item, boolean bln) {
                                super.updateItem(item, bln);
                                if (item != null) {

                                    VBox vBox = new VBox(
                                            new Text(item.getNom()),
                                            new Text(item.getType()),
                                            new Text(item.getLieu()),
                                            new Text(item.getDescription()),
                                            new Text(item.getHeure())
                                    );
                                    vBox.setSpacing(4);

                                    Image image = new Image(String.valueOf(item.getImg()), true);
                                    ImageView imv = new ImageView(image);
                                    imv.setFitHeight(130);
                                    imv.setFitWidth(130);
                                    VBox vBox2 = new VBox();
                                    Button ignorer = new Button("Ingnorer");
                                    ignorer.setOnAction((e) -> getListView().getItems().remove(getItem()));
                                    vBox2.getChildren().add(ignorer);
                                        Button btn = new Button();
        btn.setText("Participer");
        btn.setOnAction((ActionEvent event) -> {
                             try {
                                 ParticipantService es= new ParticipantService();
                                   
                                 es.createParticipant(item);
                                 getListView().getItems().remove(getItem());
//                              list.refresh();
//                               
//                                 list.getItems().clear();
                                
//                                 Node source = (Node) event.getSource();
//                                 Stage stage = (Stage) source.getScene().getWindow();
//                                 Scene scene;
//                                   scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
//                                     stage.setScene(scene);
                               
                                 
                             } catch (SQLException ex) {
                               
                             }
            
});
         list.setItems(data);  
         VBox vBox3 = new VBox();
                                   
                                    vBox3.getChildren().add(btn);
                                    HBox hBox = new HBox(imv, vBox, new Text(String.valueOf(item.getDate())), vBox2,vBox3);
                                    hBox.setSpacing(10);

                                    setGraphic(hBox);

                                }
                            }

                        };
                    }

                });
                valeur_combo_date = null;
                valeur_combo_date = null;
            } catch (SQLException ex) {

            }

        } else if (valeur_combo_events != null && valeur_combo_date == "La semaine précédente") {

            try {
                String req = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM `evttesting` WHERE type=" + "'" + valeur_combo_events + "' and DATEDIFF(NOW(),date) > 0 and DATEDIFF(NOW(),date) < 8";
                PreparedStatement ste;

                ste = DB.getInstance().getConnection().prepareStatement(req);
                ResultSet rs = ste.executeQuery();

                while (rs.next()) {

                 
                    data.add(new Evenement(rs.getInt("id"), rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"), rs.getInt("idM")));

                }
                list.getItems().addAll(data);

                list.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {

                    @Override
                    public ListCell<Evenement> call(ListView<Evenement> arg0) {
                        return new ListCell<Evenement>() {

                            @Override
                            protected void updateItem(Evenement item, boolean bln) {
                                super.updateItem(item, bln);
                                if (item != null) {

                                    VBox vBox = new VBox(
                                            new Text(item.getType()),
                                            new Text(item.getLieu()),
                                            new Text(item.getDescription()),
                                            new Text(item.getHeure())
                                    );
                                    vBox.setSpacing(4);

                                    Image image = new Image(String.valueOf(item.getImg()), true);
                                    ImageView imv = new ImageView(image);
                                    imv.setFitHeight(130);
                                    imv.setFitWidth(130);
                                    VBox vBox2 = new VBox();
                                    Button ignorer = new Button("Ingnorer");
                                    ignorer.setOnAction((e) -> getListView().getItems().remove(getItem()));
                                    vBox2.getChildren().add(ignorer);
                                        Button btn = new Button();
        btn.setText("Participer");
        btn.setOnAction((ActionEvent event) -> {
                             try {
                                 ParticipantService es= new ParticipantService();
                                   
                                 es.createParticipant(item);
                                 getListView().getItems().remove(getItem());
//                              list.refresh();
//                               
//                                 list.getItems().clear();
                                
//                                 Node source = (Node) event.getSource();
//                                 Stage stage = (Stage) source.getScene().getWindow();
//                                 Scene scene;
//                                   scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
//                                     stage.setScene(scene);
                               
                                 
                             } catch (SQLException ex) {
                            
                             }
            
});
         list.setItems(data);  
         VBox vBox3 = new VBox();
                                   
                                    vBox3.getChildren().add(btn);
                                    HBox hBox = new HBox(imv, vBox, new Text(String.valueOf(item.getDate())), vBox2,vBox3);
                                    hBox.setSpacing(10);

                                    setGraphic(hBox);

                                }
                            }

                        };
                    }

                });
                valeur_combo_date = null;
                valeur_combo_date = null;
            } catch (SQLException ex) {

            }

        } else if (valeur_combo_events != null && valeur_combo_date == "La semaine suivante") {

            try {
                String req = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM `evttesting` WHERE type=" + "'" + valeur_combo_events + "' and DATEDIFF(date,NOW()) > 0 and DATEDIFF(date,NOW()) < 8";
                PreparedStatement ste;

                ste = DB.getInstance().getConnection().prepareStatement(req);
                ResultSet rs = ste.executeQuery();

                while (rs.next()) {

                    //data.add(new Covoiturage(rs.getInt("id_cov"), rs.getInt("id_user"),rs.getDate("date_depart"),rs.getInt("heure_depart"), rs.getInt("minute_depart"),rs.getString("ville_depart"), rs.getString("adr_depart"), rs.getString("ville_arrive"),rs.getString("adr_arrive"),rs.getInt("nbr_places"),rs.getInt("cotisation"),rs.getString("description"),rs.getInt("nbr_reserve")));
                    data.add(new Evenement(rs.getInt("id"), rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"), rs.getInt("idM")));

                }
                list.getItems().addAll(data);

                list.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {

                    @Override
                    public ListCell<Evenement> call(ListView<Evenement> arg0) {
                        return new ListCell<Evenement>() {

                            @Override
                            protected void updateItem(Evenement item, boolean bln) {
                                super.updateItem(item, bln);
                                if (item != null) {

                                    VBox vBox = new VBox(
                                            new Text(item.getType()),
                                            new Text(item.getLieu()),
                                            new Text(item.getDescription()),
                                            new Text(item.getHeure())
                                    );
                                    vBox.setSpacing(4);

                                    Image image = new Image(String.valueOf(item.getImg()), true);
                                    ImageView imv = new ImageView(image);
                                    imv.setFitHeight(130);
                                    imv.setFitWidth(130);
                                    VBox vBox2 = new VBox();
                                    Button ignorer = new Button("Ingnorer");
                                    ignorer.setOnAction((e) -> getListView().getItems().remove(getItem()));
                                    vBox2.getChildren().add(ignorer);
                                        Button btn = new Button();
        btn.setText("Participer");
        btn.setOnAction((ActionEvent event) -> {
                             try {
                                 ParticipantService es= new ParticipantService();
                                   
                                 es.createParticipant(item);
                                 getListView().getItems().remove(getItem());
//                              list.refresh();
//                               
//                                 list.getItems().clear();
                                
//                                 Node source = (Node) event.getSource();
//                                 Stage stage = (Stage) source.getScene().getWindow();
//                                 Scene scene;
//                                   scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
//                                     stage.setScene(scene);
                               
                                 
                             } catch (SQLException ex) {
                                 
                             }
            
});
         list.setItems(data);  
         VBox vBox3 = new VBox();
                                   
                                    vBox3.getChildren().add(btn);
                                    HBox hBox = new HBox(vBox, new Text(String.valueOf(item.getDate())), vBox2,vBox3);
                                    hBox.setSpacing(10);

                                    setGraphic(hBox);

                                }
                            }

                        };
                    }

                });
                valeur_combo_date = null;
                valeur_combo_date = null;
            } catch (SQLException ex) {

            }

        } else if (valeur_combo_events == null && valeur_combo_date == "La semaine précédente") {

            try {
                String req = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM `evttesting` WHERE DATEDIFF(NOW(),date) > 0 and DATEDIFF(NOW(),date) < 8";
                PreparedStatement ste;

                ste = DB.getInstance().getConnection().prepareStatement(req);
                ResultSet rs = ste.executeQuery();

                while (rs.next()) {

                    try {
                        //data.add(new Covoiturage(rs.getInt("id_cov"), rs.getInt("id_user"),rs.getDate("date_depart"),rs.getInt("heure_depart"), rs.getInt("minute_depart"),rs.getString("ville_depart"), rs.getString("adr_depart"), rs.getString("ville_arrive"),rs.getString("adr_arrive"),rs.getInt("nbr_places"),rs.getInt("cotisation"),rs.getString("description"),rs.getInt("nbr_reserve")));
                        data.add(new Evenement(rs.getInt("id"), rs.getString("nom"), rs.getString("type"), rs.getDate("date"), rs.getString("lieu"), rs.getString("description"), rs.getString("heure"), rs.getString("img"), rs.getInt("idM")));

                    } catch (SQLException ex) {

                    }

                }
                list.getItems().addAll(data);

                list.setCellFactory(new Callback<ListView<Evenement>, ListCell<Evenement>>() {

                    @Override
                    public ListCell<Evenement> call(ListView<Evenement> arg0) {
                        return new ListCell<Evenement>() {

                            @Override
                            protected void updateItem(Evenement item, boolean bln) {
                                super.updateItem(item, bln);
                                if (item != null) {

                                    VBox vBox = new VBox(
                                            new Text(item.getType()),
                                            new Text(item.getLieu()),
                                            new Text(item.getDescription()),
                                            new Text(item.getHeure())
                                    );
                                    vBox.setSpacing(4);

                                    Image image = new Image(String.valueOf(item.getImg()), true);
                                    ImageView imv = new ImageView(image);
                                    imv.setFitHeight(130);
                                    imv.setFitWidth(130);
                                    VBox vBox2 = new VBox();
                                    Button ignorer = new Button("Ingnorer");
                                    ignorer.setOnAction((e) -> getListView().getItems().remove(getItem()));
                                    vBox2.getChildren().add(ignorer);
                                        Button btn = new Button();
        btn.setText("Participer");
        btn.setOnAction((ActionEvent event) -> {
                             try {
                                 ParticipantService es= new ParticipantService();
                                   
                                 es.createParticipant(item);
                                 getListView().getItems().remove(getItem());
//                              list.refresh();
//                               
//                                 list.getItems().clear();
                                
//                                 Node source = (Node) event.getSource();
//                                 Stage stage = (Stage) source.getScene().getWindow();
//                                 Scene scene;
//                                   scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
//                                     stage.setScene(scene);
                               
                                 
                             } catch (SQLException ex) {
                             
                             }
            
});
         list.setItems(data);  
         VBox vBox3 = new VBox();
                                   
                                    vBox3.getChildren().add(btn);
                                    HBox hBox = new HBox(imv, vBox, new Text(String.valueOf(item.getDate())), vBox2,vBox3);
                                    hBox.setSpacing(10);

                                    setGraphic(hBox);

                                }
                            }

                        };
                    }

                });
                valeur_combo_date = null;
                valeur_combo_date = null;
            } catch (SQLException ex) {

            }

        }

    }

    @FXML
    private void Rechercher_event1(ActionEvent event) throws IOException {

        valeur_combo_events = combo_catgorie.getValue();
        // valeur_combo_date = combo_date.getValue();
        System.out.println(valeur_combo_events);
        //   System.out.println(valeur_combo_date);
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        // stage.close();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
        stage.setScene(scene);
        // stage.show();

    }

    @FXML
    private void Rechercher_event2(ActionEvent event) throws IOException {

        // valeur_combo_events = combo_catgorie.getValue();
        valeur_combo_date = combo_date.getValue();
        // System.out.println(valeur_combo_events);
        System.out.println(valeur_combo_date);
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        // stage.close();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
        stage.setScene(scene);
        // stage.show();

    }

    @FXML
    private void showEvent(MouseEvent event) throws IOException {

        if (event.getClickCount() > 1) {
            int selectedIndex = list.getSelectionModel().getSelectedIndex();
            if ((list.getItems().get(selectedIndex)) instanceof Evenement) {

                ev = list.getItems().get(selectedIndex);
                System.out.println(ev);
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                //stage.close();
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ViewEvent.fxml")));
                stage.setScene(scene);

                //stage.show(); 
            }
        }
    }
      public static void infoBox(String infoMessage, String titleBar, String headerMessage) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }
/////Menu
    @FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ViewEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/HomeEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AddEvent.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button4(ActionEvent event) throws IOException {
Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Events.fxml")));
        stage.setScene(scene);

    }
         @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("Vous avez 3 évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
   //  notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }
}
