package GUI;

import Utils.session;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.service.directions.DirectionsService;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.controlsfx.control.Notifications;

public class INAssociationController implements Initializable, MapComponentInitializedListener {

    @FXML
    private BorderPane borderpane;

    @FXML
    private AnchorPane parent;

    @FXML
    ListView<String> LVadherent;

    @FXML
    private Label Lhello;
    @FXML
    protected GoogleMapView mapView = new GoogleMapView("en-US", "AIzaSyDYjCwpMj7OBdHHkTty6XSzc11dxfd4gLI");
    
     @FXML
    private AnchorPane APmail;

    @FXML
    private JFXTextField TFsender;

    @FXML
    private JFXPasswordField TFpasswd;

    @FXML
    private JFXTextField TFreceiver;

    @FXML
    private JFXTextField TFsubjec;

    @FXML
    private JFXTextArea TAtext;

    private Stage stage;
    

    MembreServices ms = new MembreServices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {
            Lhello.setText("Bienvenue sur " + session.getNomasso());
            Associationservices as = new Associationservices();
            int id_asso = as.getidAssoByName(session.getNomasso());
            LVadherent.setItems(as.getAllAssociationusers(id_asso));
            //System.out.println(as.getAllAssociationusers(id_asso).get(0));
        } catch (Exception e) {

        }

        mapView.addMapInializedListener(this);
//        to.bindBidirectional(toTextField.textProperty());
//        from.bindBidirectional(fromTextField.textProperty());

    }
    
    @FXML
    private void showEmailOptions() throws SQLException{
        APmail.setVisible(true);
        mapView.setVisible(false);
        String s = "";
        s += LVadherent.getSelectionModel().getSelectedItem();
        String email = s.substring(s.lastIndexOf(":")+1);
        TFreceiver.setText(email);
        TFsender.setText(ms.getEmailUSERByusername());    
    }
    
    @FXML
    void Envoyer(ActionEvent event) {
        try {
            String host = "smtp.gmail.com";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider()); //security ta java
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(TFsender.getText()));
            InternetAddress[] address = {new InternetAddress(TFreceiver.getText())}; //li yab3eth
            msg.setRecipients(Message.RecipientType.TO, address); //receiver
            msg.setSubject(TFsubjec.getText());
            msg.setSentDate(new Date());
            msg.setText(TAtext.getText());

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, TFsender.getText(), TFpasswd.getText());
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();

            Image image = new Image("/icons/mail.png");
            Notifications notification = Notifications.create()
                    .title("email envoyer !")
                    .text("votre email a été envoyer")
                    .graphic(new ImageView(image))
                    .hideAfter(Duration.seconds(7))
                    .position(Pos.TOP_RIGHT);
                    
            notification.show();
            TFpasswd.setText("");TFreceiver.setText("");TFsender.setText("");TAtext.setText("");TFsubjec.setText("");
        } catch (Exception ex) {
            System.out.println(ex);
            
            Alert alertu = new Alert(Alert.AlertType.ERROR);
            alertu.setTitle("failed!");
            alertu.setHeaderText("votre email n'est pas envoyer verifier les champs !");
            Optional<ButtonType> result = alertu.showAndWait();
        }
    }

    @FXML
    void annuler(ActionEvent event) {
        APmail.setVisible(false);
        mapView.setVisible(true);
    }

    @Override
    public void mapInitialized() {
        MapOptions options = new MapOptions();

        options.center(new LatLong(36.8065, 10.1815))
                .zoomControl(true)
                .zoom(12)
                .overviewMapControl(false)
                .mapType(MapTypeIdEnum.ROADMAP);
        GoogleMap map = mapView.createMap(options);
//        GoogleMapView mapView = new GoogleMapView("en-US", "My-Google-Map-API-Key");
//        directionsService = new DirectionsService();
//        directionsPane = mapView.getDirec();
    }

    @FXML
    void button1(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/add_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/user_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/get_all_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button4(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
        stage.setScene(scene);
    }
    
    @FXML
    private void getmeout(ActionEvent event) throws IOException, SQLException {
        Associationservices as = new Associationservices();
        MembreServices ms = new MembreServices();
        int id_asso = as.getidAssoByName(session.getNomasso());
        as.deleteAdherent(id_asso, ms.getidUSERByusername());
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/get_all_association.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) parent.getScene().getWindow();
        stagec.close();

    }

}
