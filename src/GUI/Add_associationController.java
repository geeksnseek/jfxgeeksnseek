package GUI;

import Utils.session;
import com.jfoenix.controls.JFXComboBox;
import entities.Adresse;
import entities.Association;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;

public class Add_associationController implements Initializable {

    @FXML
    private BorderPane borderpane;

    @FXML
    private Label Lhello;

    @FXML
    private AnchorPane parent;

    @FXML
    private Label Lasso;

    @FXML
    private TextField TFnom;

    @FXML
    private JFXComboBox<String> CBgovernorat;

    @FXML
    private JFXComboBox<String> CBvile;

    @FXML
    private TextField TFrue;
    @FXML
    private ImageView IVimage;

    @FXML
    private TextField TFcap;
    @FXML
    private TextArea TApath;

    Adresse a = new Adresse();

    int idlastasso = 0;

    private FileChooser fileChooser;
    private File file;
    private Image image;
    private FileInputStream fis;

    private Stage stage;

    MembreServices ms = new MembreServices();

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {
            Lhello.setText("Bonjour " + session.getUser());
        } catch (Exception e) {

        }
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        a.getGovBYid(1),
                        a.getGovBYid(2),
                        a.getGovBYid(3),
                        a.getGovBYid(4),
                        a.getGovBYid(5),
                        a.getGovBYid(6),
                        a.getGovBYid(7),
                        a.getGovBYid(8),
                        a.getGovBYid(9),
                        a.getGovBYid(10),
                        a.getGovBYid(11),
                        a.getGovBYid(12)
                );
        CBgovernorat.setItems(options);

    }

    @FXML
    private void Parcourir(ActionEvent event) throws Exception {

        fileChooser = new FileChooser();

        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image files", "*.png", "*.jpg", "*.gif"));
        Stage stagec = (Stage) parent.getScene().getWindow();
        file = fileChooser.showOpenDialog(stagec);
        if (file != null) {
            //desktop.open(file);
            image = new Image(file.toURI().toString());
            IVimage.setImage(image);
            TApath.setText(file.getAbsolutePath());

        }
    }

    @FXML
    void button1() throws IOException {
        //current
    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/user_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/get_all_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button4(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/adherentlist.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void onAdressConfirmed(ActionEvent event) {
        int id = CBgovernorat.getSelectionModel().getSelectedIndex() + 1;
        System.out.println(id);
        CBvile.setItems(a.getVilleFroGov(id));

    }

    @FXML
    void onCreerClicked(ActionEvent event) throws SQLException, FileNotFoundException {
        
        java.sql.Date d = new java.sql.Date(119, 1, 15);
        //System.out.println(d);
        a.setGov(CBgovernorat.getSelectionModel().getSelectedItem());
        a.setRue(TFrue.getText());
        a.setVille(CBvile.getSelectionModel().getSelectedItem());
        System.out.println(a.toString());
        String s = a.toString();
        Associationservices as = new Associationservices();
        MembreServices m = new MembreServices();
        
        if (!TFnom.getText().isEmpty() && !TFrue.getText().isEmpty() && !TFrue.getText().isEmpty()
                && !CBvile.getSelectionModel().isEmpty() && !CBgovernorat.getSelectionModel().isEmpty()
                && !TApath.getText().isEmpty()) {
            double c = Double.parseDouble(TFcap.getText());
            fis = new FileInputStream(file);
            Association a = new Association(TFnom.getText(), s, c, m.getidUSERByusername(), (InputStream) fis);
            idlastasso = as.createAsso(a);
            as.addadherent(idlastasso, ms.getidUSERByusername());
            System.out.println(idlastasso);
        } else {
            Alert alertu = new Alert(Alert.AlertType.ERROR);
        alertu.setTitle("Champs manquant!");
        alertu.setHeaderText("Veuillez saisir les champs requis !");
        Optional<ButtonType> result = alertu.showAndWait();
        }

//        Alert alertu = new Alert(Alert.AlertType.ERROR);
//        alertu.setTitle("success !");
//        alertu.setHeaderText("félicitationt association créer avec succes !");
//        Optional<ButtonType> result = alertu.showAndWait();

    }

    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml")));
        stage.setScene(scene);

    }

}
