package GUI;

import Utils.session;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.apache.http.auth.AUTH;
import org.controlsfx.control.Notifications;

public class EventsController implements Initializable{

    @FXML
    private MediaView media;
    private MediaPlayer mediaPlayer;
   private static final String MEDIA_URL = "/vid/Earth.mp4";
@FXML
private WebView webView;
 @FXML
    private Label Lwelcome;
    private WebEngine engine;
    
URL location;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Lwelcome.setText("Bonjour "+session.getUser());
        } catch (Exception e) {
        }
        System.out.println(location.toString());
        System.out.println(this.getClass().getResource(MEDIA_URL).toExternalForm());
//        mediaPlayer = new MediaPlayer(new Media(this.getClass().getResource(MEDIA_URL).toExternalForm()));
//        mediaPlayer.setAutoPlay(true);
//        media.setMediaPlayer(mediaPlayer);
        engine = webView.getEngine();
         engine.load("https://web.facebook.com/emna.debbabi");
    }

///////Menu
    
    @FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ViewEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/HomeEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AddEvent.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button4(ActionEvent event) throws IOException {
    Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Events.fxml")));
        stage.setScene(scene);

    }
       @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("Vous avez 3 évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
   //  notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }

}
