package GUI;
import Utils.*;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

public class CommandeController implements Initializable {

    @FXML
    private BorderPane borderpane;
    
     @FXML
    private AnchorPane parent;

    @FXML
    private Pane pane1;

    @FXML
    private Pane pane2;

    @FXML
    private Pane pane3;
    
    @FXML
    private Label Lusername;


    private Stage stage;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Lusername.setText("hello "+session.getUser());
            System.out.println(session.getUser());
        } catch (Exception e) {
        }


    }
    
  

    @FXML
    void button1() throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/GUI/Categorie.fxml"));
         //dima taille l ficjier fxml = 824x630
         parent.getChildren().setAll(pane);
         

    }

    @FXML
    void button2() throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/GUI/Panier.fxml"));
         
         parent.getChildren().setAll(pane);

    }

    @FXML
    void button3() throws IOException
    {
       AnchorPane pane = FXMLLoader.load(getClass().getResource("/GUI/Rappel.fxml"));
         
         parent.getChildren().setAll(pane);
    }

    @FXML
    void button4() {

    }
    
    @FXML
    void backToMenu() throws IOException {
        Parent root =FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene=new Scene(root,1024,768);
        Stage stage= new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) pane1.getScene().getWindow();
        stagec.close();

    }


}
