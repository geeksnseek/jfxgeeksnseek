/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.math.BigInteger; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import entities.Membre;
import entities.Adresse;
import Utils.*;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import services.MembreServices;

/**
 *
 * @author med
 */
public class LoginController implements Initializable {

    @FXML
    private Pane Pane1;

    @FXML
    private Pane Pane2;

    @FXML
    private Pane Pane3;

    @FXML
    private Pane Pane4;

    @FXML
    private Pane popup;

    @FXML
    private JFXTextField TFlogin;

    @FXML
    private JFXPasswordField TFpasswd;

    @FXML
    private JFXTextField TFnom;

    @FXML
    private JFXTextField TFprenom;

    @FXML
    private JFXTextField TFemail;

    @FXML
    private JFXTextField TFntelephone;

    @FXML
    private JFXComboBox<String> CBgov;

    @FXML
    private JFXComboBox<String> CBville;

    @FXML
    private JFXTextField TFrue;

    @FXML
    private DatePicker DPdatenaissance;

    @FXML
    private RadioButton Rf;

    @FXML
    private RadioButton Rh;

    @FXML
    private JFXTextField TFnomuti;

    @FXML
    private JFXPasswordField Tfmotdepasse;

    final private ToggleGroup tg = new ToggleGroup();
    Adresse a = new Adresse();
    ControleSaisie cs = new ControleSaisie();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        a.getGovBYid(1),
                        a.getGovBYid(2),
                        a.getGovBYid(3),
                        a.getGovBYid(4),
                        a.getGovBYid(5),
                        a.getGovBYid(6),
                        a.getGovBYid(7),
                        a.getGovBYid(8),
                        a.getGovBYid(9),
                        a.getGovBYid(10),
                        a.getGovBYid(11),
                        a.getGovBYid(12)
                );
        CBgov.setItems(options);

        Rh.setToggleGroup(tg);

        Rf.setToggleGroup(tg);
        Rf.setSelected(true);

        Pane1.setStyle("-fx-background-image: url(\"/images/111.jpg\")");
        Pane2.setStyle("-fx-background-image: url(\"/images/222.jpg\")");
        Pane3.setStyle("-fx-background-image: url(\"/images/333.jpg\")");
        Pane4.setStyle("-fx-background-image: url(\"/images/111.jpg\")");

        backgroundAnimation();
    }

    @FXML
    private void govChanged() {
        int id = CBgov.getSelectionModel().getSelectedIndex() + 1;
        System.out.println(id);
        CBville.setItems(a.getVilleFroGov(id));
    }

    private void backgroundAnimation() {

        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(3), Pane4);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.play();

        fadeTransition.setOnFinished(event -> {

            FadeTransition fadeTransition1 = new FadeTransition(Duration.seconds(3), Pane3);
            fadeTransition1.setFromValue(1);
            fadeTransition1.setToValue(0);
            fadeTransition1.play();

            fadeTransition1.setOnFinished(event1 -> {
                FadeTransition fadeTransition2 = new FadeTransition(Duration.seconds(3), Pane2);
                fadeTransition2.setFromValue(1);
                fadeTransition2.setToValue(0);
                fadeTransition2.play();

                fadeTransition2.setOnFinished(event2 -> {

                    FadeTransition fadeTransition0 = new FadeTransition(Duration.seconds(3), Pane2);
                    fadeTransition0.setToValue(1);
                    fadeTransition0.setFromValue(0);
                    fadeTransition0.play();

                    fadeTransition0.setOnFinished(event3 -> {

                        FadeTransition fadeTransition11 = new FadeTransition(Duration.seconds(3), Pane3);
                        fadeTransition11.setToValue(1);
                        fadeTransition11.setFromValue(0);
                        fadeTransition11.play();

                        fadeTransition11.setOnFinished(event4 -> {

                            FadeTransition fadeTransition22 = new FadeTransition(Duration.seconds(3), Pane4);
                            fadeTransition22.setToValue(1);
                            fadeTransition22.setFromValue(0);
                            fadeTransition22.play();

                            fadeTransition22.setOnFinished(event5 -> {
                                backgroundAnimation();
                            });

                        });

                    });

                });
            });

        });

    }

    @FXML
    void onSignInClicked(ActionEvent event) throws IOException {
        if(TFlogin.getText().equals("admin")){
            popup.setVisible(false);
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/MenuAdmin.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) Pane1.getScene().getWindow();
        stagec.close();
        session.setUser(TFlogin.getText());
        }else{
            popup.setVisible(false);
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene = new Scene(root, 1024, 768);
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) Pane1.getScene().getWindow();
        stagec.close();
        session.setUser(TFlogin.getText());
        }
        

    }

    @FXML
    void onSignupClicked(ActionEvent event) {
        popup.setVisible(true);
        if (Rf.isSelected()) {
            System.out.println("hello");
        };

    }

    @FXML
    private void onSinscrire() throws IOException {

        a.setGov(CBgov.getSelectionModel().getSelectedItem());
        a.setRue(TFrue.getText());
        a.setVille(CBville.getSelectionModel().getSelectedItem());
        String sex = "";
        if (Rf.isSelected()) {
            sex = "s:5:\"Femme\";";
        } else {
            sex = "a:1:{i:0;s:5:\"Homme\";}";
        }

        try {
            MembreServices ms = new MembreServices();

            if (!TFnom.getText().isEmpty() && !TFprenom.getText().isEmpty() && !TFemail.getText().isEmpty() && !TFntelephone.getText().isEmpty()
                    && !TFnomuti.getText().isEmpty() && !Tfmotdepasse.getText().isEmpty() && !TFrue.getText().isEmpty()) {
                if (cs.mailformat(TFemail.getText())) {
                    if (cs.GSM(TFntelephone.getText())) {
                        if (!ms.usernameExist(TFnomuti.getText())) {
                            LocalDate locald;
                            locald = DPdatenaissance.getValue();
                            Date date = Date.valueOf(locald);
                            Membre m = new Membre(TFnom.getText(), TFprenom.getText(), sex, date, TFemail.getText(), a.toString(),
                                    Integer.parseInt(TFntelephone.getText()), TFnomuti.getText(),"$2y$13$F5Hhm5nlpesoJsxlBNxET.aYjoy/AsfGVdHtw6xKlmDttrpV1K5IC", "user");
                            ms.createUSER(m);
                            Parent root = FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
                            Scene scene = new Scene(root, 1024, 768);
                            Stage stage = new Stage(StageStyle.DECORATED);
                            stage.setScene(scene);
                            stage.show();
                            Stage stagec = (Stage) Pane1.getScene().getWindow();
                            stagec.close();
                            session.setUser(TFnomuti.getText());

                        } else {
                            Alert alertu = new Alert(Alert.AlertType.ERROR);
                            alertu.setTitle("in use!");
                            alertu.setHeaderText("le nom d'utilisateur existe déja !");
                            Optional<ButtonType> result = alertu.showAndWait();

                        }
                    } else {
                        Alert alert3 = new Alert(Alert.AlertType.ERROR);
                        alert3.setTitle("numero non valide!");
                        alert3.setHeaderText("Veuillez saisir numero de telephone valide !");
                        Optional<ButtonType> result = alert3.showAndWait();

                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("mail non valide!");
                    alert.setHeaderText("Veuillez saisir mail valide !");
                    Optional<ButtonType> result = alert.showAndWait();

                }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR);
                alert2.setTitle("Champs manquant!");
                alert2.setHeaderText("Veuillez saisir les champs requis !");
                Optional<ButtonType> result = alert2.showAndWait();
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

    }
    public static String encryptThisString(String input) 
    { 
        try { 
            // getInstance() method is called with algorithm SHA-512 
            MessageDigest md = MessageDigest.getInstance("SHA-512"); 
  
            // digest() method is called 
            // to calculate message digest of the input string 
            // returned as array of byte 
            byte[] messageDigest = md.digest(input.getBytes()); 
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
  
            // Add preceding 0s to make it 32 bit 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
  
            // return the HashText 
            return hashtext; 
        } 
  
        // For specifying wrong message digest algorithms 
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    } 
    public String get_SHA_512_SecurePassword(String passwordToHash){
String generatedPassword = null;
    try {
         MessageDigest md = MessageDigest.getInstance("SHA-512");
         //md.update(salt.getBytes(StandardCharsets.UTF_8));
         byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
         StringBuilder sb = new StringBuilder();
         for(int i=0; i< bytes.length ;i++){
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
         }
         generatedPassword = sb.toString();
        } 
       catch (NoSuchAlgorithmException e){
        e.printStackTrace();
       }
    return generatedPassword;
}
}
