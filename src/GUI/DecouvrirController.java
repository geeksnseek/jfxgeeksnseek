package GUI;

import database.DB;
import static database.DB.getInstance;
import entities.Evenement;
import entities.Membre;
import entities.Participant;
import Utils.session;
//import static GUI.HomeEventController.z;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoClientException;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.SmsSubmissionResponseMessage;
import com.nexmo.client.sms.messages.TextMessage;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.controlsfx.control.Notifications;
import services.MembreServices;
import services.ParticipantService;
import static sun.security.krb5.Confounder.intValue;

public class DecouvrirController implements Initializable{
    MembreServices MS=new MembreServices();
    @FXML
    private Label msg;
    @FXML
    private Label cadeau;
   
 @FXML
    private Pane paneenv;

    @FXML
    private TextArea contenu;

    @FXML
    private TextField numerooo;

    @FXML
    private JFXButton envoi;

    @FXML
    private Label monemail;

    @FXML
    private Label lt;

    @FXML
    private TextField sujet;

    @FXML
    private Pane p2;

    @FXML
    private JFXRadioButton sms;

    @FXML
    private Label username;

    @FXML
    private Pane p1;

    @FXML
    private JFXRadioButton smail;
@FXML
    private Label Lwelcome;
    
      Membre connect = new Membre(16, "Emna", "Debbabi", "femme", new Date(1995, 9, 5), "debbabiemna5@gmail.com", "Nabeul", 52978520, "emna5", "emna591995", "Membre");
       Membre owner = new Membre(16, "Emna", "Debbabi", "femme", new Date(1995, 9, 5), "debbabiemna5@gmail.com", "Nabeul", 52978520, "emna5", "emna591995", "Administrateur");
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Lwelcome.setText("Bonjour "+session.getUser());
   username.setText(owner.getEmail());
        // TODO
        paneenv.setVisible(false);       
        monemail.setText(connect.getEmail());

//            p1.setVisible(true);
//            
//            p2.setVisible(true);
//            smail.setSelected(false);
//            paneenv.setVisible(false);
//            paneenv.setStyle("-fx-background-color:  #fff5f5;");
//  if (smail.isSelected()) {
//       p2.setVisible(true);
//      paneenv.setVisible(true);}
//   if (smail.isSelected()) {
//       p1.setVisible(false);
//      paneenv.setVisible(true);}
           numerooo.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
                public void handle(KeyEvent t) {
                    char ar[] = t.getCharacter().toCharArray();
                    char ch = ar[t.getCharacter().toCharArray().length - 1];
                    if (!(ch >= '0' && ch <= '9')) {
                        t.consume();
                    }

                    int maxCharacters = 8;
                    if (numerooo.getText().length() > maxCharacters) {
                        t.consume();
                       contenu.setText("nom max 20 c");
                    }
                }
            });
           smail.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(smail.isSelected())
                      
                 paneenv.setVisible(true);
            }
           
           });
           sms.selectedProperty().addListener(new ChangeListener<Boolean>(){
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(sms.isSelected())
                    
                 paneenv.setVisible(true);
            }
           
           });      
    }
      
    @FXML
    private void envoyer(ActionEvent event) throws IOException, NexmoClientException, AddressException, MessagingException,SQLException {
    
                if(smail.isSelected()){
                      
       if (contenu.getText().length() != 0){         

       try {
           final String username =connect.getEmail();
           final String password =connect.getMotdepasse();
           Properties props = new Properties();
           props.put("mail.smtp.auth", "true");
           props.put("mail.smtp.starttls.enable", "true");
           props.put("mail.smtp.host", "smtp.gmail.com");
           props.put("mail.smtp.port", "587");
           
           Session session = Session.getInstance(props,
                   new javax.mail.Authenticator() {
                       @Override
                       protected PasswordAuthentication getPasswordAuthentication() {
                           return new PasswordAuthentication(username, password);
                       }
                   });
           Message message = new MimeMessage(session);
//        message.setFrom(new InternetAddress("dhiabsdl94@gmail.com"));
message.setFrom(new InternetAddress(connect.getEmail()));
message.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse(owner.getEmail()));

message.setSubject(sujet.getText());
message.setText(contenu.getText());

Transport.send(message);

System.out.println("Done");

Notifications.create().hideAfter(Duration.seconds(2)).position(Pos.CENTER).text("email envoyer").showConfirm();

////////////
       } catch (MessagingException ex) {
          
       }
                    
                   }
else{Notifications.create().hideAfter(Duration.seconds(5)).position(Pos.CENTER).text("email non envoyer verifier votre connection").showError(); 
        }
                }
        
                 
           
           
  if (sms.isSelected()){
  if((numerooo.getText().length()>0)&&(numerooo.getText().length()<8)&&(contenu.getText().length() != 0))
  {
            

 NexmoClient client = new NexmoClient.Builder()
  .apiKey("af38776e")
  .apiSecret("XMxqOZ4zU8tAu4F9")
  .build();

String messageText = contenu.getText();
TextMessage message = new TextMessage("Nexmo", "21652978520", messageText);

SmsSubmissionResponse response = client.getSmsClient().submitMessage(message);

for (SmsSubmissionResponseMessage responseMessage : response.getMessages()) {
    System.out.println(responseMessage);}
Notifications.create().hideAfter(Duration.seconds(2)).position(Pos.CENTER).text("sms envoyer").showConfirm();
}

 else{ Notifications.create().hideAfter(Duration.seconds(5)).position(Pos.CENTER).text("sms non envoyer \n verfier votre connection").showError();}                 

                   } 
        }
       @FXML
    private void numerooosaisie(KeyEvent t ) {
        
       
                    char ar[] = t.getCharacter().toCharArray();
                    char ch = ar[t.getCharacter().toCharArray().length - 1];
                    if (!(ch >= '0' && ch <= '9')) {
                        t.consume();
                    }

       
                    int maxCharacters = 8;
                    if (numerooo.getText().length() > maxCharacters) {
                        t.consume();
                      //  tfd.setText("nom max 8 chiffre");
                    
    }}

    @FXML
    public void Score(ActionEvent event) throws SQLException, FileNotFoundException, DocumentException {
        Connection connection;
        DB db = getInstance();
        connection = db.getConnection();
//  String  req="SELECT count(*) FROM  `participant` WHERE idM="+String.valueOf(z.getIdM());
//     PreparedStatement ps = connection.prepareStatement(req);
//   // ps.setInt(1,13);
//       ResultSet rs=ps.executeQuery();
//        System.out.println(rs);
//        if ( (((Number) rs.getObject(1)).intValue())>5){
//        msg.setText("Pas assez actif!");
        ObservableList<Participant> list = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("SELECT idP,idM,idevent,etat,avis FROM `participant` WHERE idM=" + MS.getidUSERByusername());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            ParticipantService k = new ParticipantService();
            list.add(k.mapResultsToParticipant(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5)));
            if (list.size() >= 5) {
                msg.setText("Bravo! Vous êtes bien actif et conscient de notre environnement");
                cadeau.setText("Cliquez ici pour obtenir le code de ton cadeau");
            }
            if (list.size() <= 2) {
                msg.setText("Pas assez actif!");
                cadeau.setText("Rejoinez nos évènements pour une une planète meilleur");
            }
            if ((list.size() > 2) && (list.size() <= 4)) {
                msg.setText("Assez actif!Vous êtes un ami de la planète ");
                cadeau.setText("Vous êtes très proche! Rejoinez de plus nos évènements");
            }
        }
    }

    @FXML
    private void imprimerCode(MouseEvent event) throws DocumentException, FileNotFoundException, IOException, SQLException {

        String DEST = "code.pdf";
        String nom = "";
        String prenom = "";
        int id = 0;
        Random rand = new Random();

        // extraire le user  qui a fait le offre 
        // String req = "SELECT u.nom,u.prenom,u.idM  FROM membre u join participant r on  u.idM= r.idM  ";
        String req = "SELECT idP,idM,idevent,etat,avis FROM `participant`  ";
        PreparedStatement ste;

        try {
            ste = DB.getInstance().getConnection().prepareStatement(req);
            ResultSet rss = ste.executeQuery();
            System.out.println(rss);
             
            while (rss.next()) {

//                nom = rss.getString("nom");
//                prenom = rss.getString("prenom");
//                id = rss.getInt("id_user");
                nom = rss.getString("etat");
                prenom = rss.getString("avis");
                id = rss.getInt("idM");
                System.out.println(nom);
                System.out.println(prenom);

            }

        } catch (SQLException ex) {

        }

        // mettre page size
        Rectangle small = new Rectangle(290, 100);

        Font smallfont = new Font(Font.FontFamily.HELVETICA, 10);
        Document document = new Document(small, 5, 5, 5, 5);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(DEST));
        document.open();
        //creation table 
        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(new float[]{160, 120});
        table.setLockedWidth(true);
        PdfContentByte cb = writer.getDirectContent();
         Membre z = MS.getUSERByusername();
        // first row
//        PdfPCell cell = new PdfPCell(new Phrase("Ticket Avec mr/mme "+z.getNom()+" "+z.getPrenom()+" "+z.getDatedenaissance()+" email : "+z.getEmail()+" tel : "+z.getTelephone()));
        PdfPCell cell = new PdfPCell(new Phrase("Code cadeau de mr/mme " + z.getNom() + " " + z.getPrenom() + " " + z.getDatenaissance() + " email : " + z.getEmail() + " tel : " + z.getTelephone()));
        cell.setFixedHeight(30);
        cell.setBorder(Rectangle.BOX);
        cell.setColspan(2);
        table.addCell(cell);

        // second row
        cell = new PdfPCell(new Phrase("Adresse : " + z.getAdresse(), smallfont));
        cell.setFixedHeight(30);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        //creation de barcode
        Barcode128 code128 = new Barcode128();
        code128.setCode(String.valueOf(MS.getidUSERByusername() * 100 + id * 2598 + 258120941));
        code128.setCodeType(Barcode128.CODE128);
        //generation de barcode
        Image code128Image = code128.createImageWithBarcode(cb, null, null);
        cell = new PdfPCell(code128Image, true);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(30);
        table.addCell(cell);
        // third row
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(" Code cadeau pour mr/mme  " + nom + " " + prenom, smallfont));
        cell.setBorder(Rectangle.BOX);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);
        document.add(table);

        document.close();
        //creation de pdf
        File file = new File(DEST);

        try {

            Desktop.getDesktop().open(new java.io.File("code.pdf"));

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

//        Node source = (Node) event.getSource();
//            Stage stage = (Stage) source.getScene().getWindow();
        // stage.close();
        /*Scene scene = new Scene(FXMLLoader.load(getClass().getResource("AfficheCovOffre.fxml")));
            stage.setScene(scene);
            stage.show();*/
    }
  @FXML
    void stat(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Dashboard1.fxml")));
        stage.setScene(scene);

    }
///////Menu
    @FXML
    void button1(ActionEvent event) throws IOException {

        //dima taille l ficjier fxml = 824x630
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/ViewEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/HomeEvent.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/AddEvent.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void button4(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/Decouvrir.fxml")));
        stage.setScene(scene);
    }

    @FXML
    void backToMenu(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/login.fxml")));
        stage.setScene(scene);

    }
     @FXML
    void Notifications(ActionEvent event) throws IOException {
           System.out.println("dddddddd");
           Notifications notificationBuilder;
        notificationBuilder = Notifications.create()
        .title("Evènement à venir")
                 .text("Vous avez 3 évènements aujourd'hui ")
                .graphic(null)
                 .hideAfter(Duration.seconds(5))
                 .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>(){
               @Override
               public void handle(ActionEvent event) {
                   System.out.println("Clicked on Notification");
               }
    
    });
   //  notificationBuilder.darkStyle();
     notificationBuilder.showConfirm();

    }
}
