package GUI;

import Utils.session;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import services.*;
import java.net.URL;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

public class AdheerentlistController implements Initializable {

    @FXML
    private BorderPane borderpane;
    
     @FXML
    private AnchorPane parent;

    @FXML
    private Label Lhello;
    
    @FXML
    private ListView<String> LVlist ;
   


    private Stage stage;
    
    MembreServices ms = new MembreServices();
    
    


    @Override
    public void initialize(URL url, ResourceBundle resources) {
        try {
            Lhello.setText("Bonjour "+session.getUser());
            MembreServices ms = new MembreServices();
            Associationservices as = new Associationservices();
            LVlist.setItems(as.getAllUserAssociations(ms.getidUSERByusername()));
            
        } catch (Exception e) {
        }
     
    }
    
    @FXML
    private void unsubscribe(){
        
    }
    
    
  

    @FXML
    void button1(ActionEvent event) throws IOException {
       Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/add_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button2(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/user_association.fxml")));
        stage.setScene(scene);


    }

    @FXML
    void button3(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/get_all_association.fxml")));
        stage.setScene(scene);

    }

    @FXML
    void button4() {
        //current
    }
    
    @FXML
    void backToMenu() throws IOException {
        Parent root =FXMLLoader.load(getClass().getResource("/GUI/Menu.fxml"));
        Scene scene=new Scene(root,1024,768);
        Stage stage= new Stage(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
        Stage stagec = (Stage) parent.getScene().getWindow();
        stagec.close();

    }

    


}
