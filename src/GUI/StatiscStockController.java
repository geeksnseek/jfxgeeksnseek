/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.Pane;
import services.StockService;

/**
 * FXML Controller class
 *
 * @author brini
 */
public class StatiscStockController implements Initializable {

     @FXML
    private Pane Pchart;

    private PieChart pieChart ;
    private final ObservableList<PieChart.Data> details = FXCollections.observableArrayList();
    
   StockService service = new StockService();
    
     List<Double> qte = service.QTeCategorie();
     List<String> desc = service.descCategorie();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
   {
        
       
       for(int i =0 ;i< service.getALLCatégorie().size(); i++)
       {
           details.add(new PieChart.Data(desc.get(i), qte.get(i)));
       
        
                

          
    }  
   
          pieChart = new PieChart();
          pieChart.setData(details);
          pieChart.setTitle("Stock Disponible");
          pieChart.setLegendSide(Side.BOTTOM);
          pieChart.setLabelsVisible(true);
          Pchart.getChildren().add(pieChart);
         
   } 
    
}
