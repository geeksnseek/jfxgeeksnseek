/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import database.DB;
import entities.Catégorie;
import static database.DB.getInstance;

import entities.Panier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author brini
 */
public class StockService 
{

  
    Connection cnx;
    DB db = getInstance();
    Statement statement;
    ResultSet result;
//     PreparedStatement pst ;

    public StockService() {
    cnx = db.getConnection();

    }

    public List<Integer> Catid(int id1) throws SQLException {
        MembreServices ms =new MembreServices();
        int id = 0;
        String req = "Select stock.id_cat , stock.id_cat  from stock inner join panier on stock.id_cat = panier.id_cat where panier.id_membre = " +ms.getidUSERByusername() ;
        List<Integer> list = new ArrayList<Integer>();

        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {

                id = result.getInt(1);
                list.add(id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public List<Integer> CatidBase() {
        int id = 0;
        String req = "Select id_cat from categorie";
        List<Integer> list = new ArrayList<Integer>();

        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {

                id = result.getInt(1);
                list.add(id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;

    }

    public Catégorie getById(int id) {
        Catégorie c = null;
        String req = "Select * from categorie where id_cat =" + id;
        try {

            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            if (result.next()) {

                c = new Catégorie(result.getInt(1), result.getString(2), result.getDouble(3), result.getDouble(4));
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;
    }

    public ObservableList<Catégorie> getALLCatégorie() {
        ObservableList<Catégorie> resultat = FXCollections.observableArrayList();
        try {
            statement = cnx.createStatement();
            result = statement.executeQuery("select * from stock ");

            while (result.next()) {

                Catégorie c = new Catégorie();
                c.setId(result.getInt("id_cat"));
         
               c.setDescription(result.getString("description"));
                c.setPrixuni(result.getDouble("prixuni"));
               
                c.setQuantitedispo(result.getDouble("quantitedispo"));

                resultat.add(c);

            }
        } catch (SQLException ex) {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultat;
    }

    public List<String> descCategorie() {

        List<String> list = new ArrayList<String>();
        String req = "select description from stock";

        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {

                list.add(result.getString("description"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;

    }
    
    public ObservableList<String> descSearchCategorie(String desc) throws SQLException 
    {


        ObservableList<String> list = FXCollections.observableArrayList();
        PreparedStatement req = cnx.prepareStatement("select description from stock where description like ?");
        req.setString(1, "%"+desc+"%");
        result = req.executeQuery();
            while (result.next()) {

                list.add(result.getString("description"));

            }
       

        return list;

    }

    public double PrixuniCat(int id) {
        double prix = 0.0;
        String req = "select prixuni from stock where id_cat =" + id;
        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {
                prix = result.getDouble("prixuni");
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return prix;
    }

    public double QtedispoCat(int id) {
        double qte = 0.0;
        String req = "select quantitedispo from stock where id_cat =" + id;
        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {
                qte = result.getDouble("quantitedispo");
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return qte;
    }

    public boolean updateStockdec(int cat, double quantite) {
        String req = "update stock set quantitedispo = quantitedispo - " + quantite + " where stock.id_cat =" + cat;
        try {
            statement = cnx.createStatement();
            statement.executeUpdate(req);
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    

    public boolean updateStockinc(int cat, double quantite) {
        String req = "update stock set quantitedispo = quantitedispo + " + quantite + " where stock.id_cat =" + cat;
        try {
            statement = cnx.createStatement();
            statement.executeUpdate(req);
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
     public boolean updateStock(int cat, double quantite) {
        String req = "update stock set quantitedispo =  " + quantite + " where stock.id_cat =" + cat;
        try {
            statement = cnx.createStatement();
            statement.executeUpdate(req);
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    public void ajouterCategotire(Catégorie c) {

        try {
            Statement st = cnx.createStatement();
            String request = "INSERT INTO `stock` (   `quantitedeispo`,description , `prixuni`) VALUES ( '" + c.getQuantitedispo() + "', '" + c.getDescription() + "', '" + c.getPrixuni() + "')";

            st.executeUpdate(request);

            System.out.println("l'ajout de stock est effectué avec succés");

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
          public void UpdateSotck(Catégorie c ) 
      {
          String req ="update stock set quantitedispo = "+c.getQuantitedispo()+" ,prixuni ="+c.getPrixuni()+"where id_cat ="+c.getId();
        try
        {
            statement = cnx.createStatement();
           statement.executeUpdate(req);
        
           
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

      
     }
        
     public List<Double> QTeCategorie() {

        List<Double> list = new ArrayList<>();
        String req = "select quantitedispo from stock";

        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {

                list.add(result.getDouble("quantitedispo"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;

    }      
          
         public void create(Catégorie c) 
       
    {
        
        
        try
        { 
            
            PreparedStatement ps =cnx.prepareStatement( "INSERT INTO `stock` ( `description`,  `quantitedispo`, `prixuni`) VALUES (?,?,?)");
            ps.setString(1, c.getDescription());
            ps.setDouble(2, c.getQuantitedispo());
            ps.setDouble(3, c .getPrixuni());
            ps.executeUpdate();
            result = ps.executeQuery();
            result.next();
            

        } catch (SQLException ex) 
        {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }     
          
       
        public void delete(Catégorie c)
        {
                  String req = "delete from stock where categorie.id_cat="+c.getId();
       
        
            try 
            {
               statement = cnx.createStatement();
                statement.executeUpdate(req);
              
            } catch (SQLException ex)
            {
                Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
