/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import database.DB;
import static database.DB.getInstance;
import entities.Evenement;
import entities.Membre;
import entities.Participant;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author debba
 */
public class ParticipantService {
   Connection connection;
    DB db = getInstance();
    MembreServices MS=new MembreServices();
   
    

    private final String CREATE_PARTICIPANT = "INSERT INTO participant(idM,idevent,etat,avis) VALUES(?,?,?,?)";
    private final String GET_PARTICIPANT_BY_ID = "SELECT idP,idM,idevent,etat,avis FROM participant WHERE idM=?";
    private final String GET_PARTICIPANT_BY_IDEVENT = "SELECT idP,idM,idevent,etat,avis FROM participant WHERE idevent=?";
    private final String GET_ALL_PARTICIPANT = "SELECT idP,idM,idevent,etat,avis FROM participant";
    private final String DELETE_PARTICIPANT_BY_ID = "DELETE FROM participant WHERE idM=?";
    private final String UPDATE_PARTICIPANT = "UPDATE participant SET etat=?,avis=? WHERE idM=?";
     private final String GET_PARTICIPANT_BY_NOM = "SELECT idP,idM,idevent,etat,avis FROM participant WHERE idM=?";

    public ParticipantService() {
    connection=db.getConnection();
    }
   
//    public void createParticipant(int idM,int idevent,String etat,String avis) throws SQLException {
//        PreparedStatement ps = connection.prepareStatement(CREATE_PARTICIPANT);
//        ps.setInt(1,idM);
//         ps.setInt(2, idevent);
//        ps.setString(3,etat);
//        ps.setString(4,avis);
//        ps.executeUpdate();
//        System.out.println("Participant created");
//    }
//   
   
 public void createParticipant( Evenement e) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(CREATE_PARTICIPANT);
       ps.setInt(1,MS.getidUSERByusername());
         ps.setInt(2,e.getId());
        ps.setString(3,"interessé");
        ps.setString(4,"pas encore");
      

        ps.executeUpdate();
        System.out.println("Participant created");
    }
   
//    public void createEvenement(int id, String nom, Date date, double cap) throws SQLException {
//        PreparedStatement ps = connection.prepareStatement(CREATE_EVENEMENT);
//        ps.setInt(1, id);
//        ps.setString(2, nom);
//        ps.setDate(3, date);
//        ps.setDouble(4, cap);
//      
//
//        ps.executeUpdate();
//        System.out.println("Participant created");
//    }

   
    public Participant mapResultsToParticipant(int idP,int idM,int idevent,String etat,String avis) {
         return new Participant(idP,idM,idevent,etat,avis);
    }

  
    public Participant getParticipantById(int idM)throws SQLException {
          PreparedStatement ps = connection.prepareStatement(GET_PARTICIPANT_BY_ID);
        ps.setInt(1, idM);
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
         System.out.println("le participant correspondant est:");
        return mapResultsToParticipant(rs.getInt(1),rs.getInt(2),rs.getInt(3), rs.getString(4),rs.getString(5) );
    }

 
    public ObservableList<Participant> getAll()throws SQLException {
          ObservableList<Participant> event = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement(GET_ALL_PARTICIPANT);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            event.add(mapResultsToParticipant(rs.getInt(1),rs.getInt(2),rs.getInt(3), rs.getString(4),rs.getString(5)));
        }
 System.out.println("la liste des participants:");
        return event;
    }

 
    public void deleteParticipantById(int idM) throws SQLException{
       PreparedStatement ps = connection.prepareStatement(DELETE_PARTICIPANT_BY_ID);
        ps.setInt(1, idM);
        ps.executeUpdate();
        System.out.println("participant deleted");
    }

    public void updateParticipant(int idP,int idM,int idevent,String etat,String avis)throws SQLException {
         PreparedStatement ps = connection.prepareStatement(UPDATE_PARTICIPANT);

        ps.setString(1,etat);
       
        ps.setString(2,avis);
     
      ps.setInt(3, idM);

        ps.executeUpdate();
        System.out.println("updated ");
    }

 
//    public Evenement getParticipantByNom(String nom) throws SQLException {
//           PreparedStatement ps = connection.prepareStatement(GET_EVENEMENT_BY_NOM);
//        ps.setString(1, nom);
//        ResultSet rs = ps.executeQuery();
//        rs.next();//next return boolean
//         System.out.println("l'evenement correspondant est:");
//        return  mapResultsToParticipant(rs.getInt(1),rs.getInt(2),rs.getInt(3), rs.getString(4),rs.getString(5) );
//    }
   public boolean getParticipantByIdevent(int idevent)throws SQLException {
          PreparedStatement ps = connection.prepareStatement(GET_PARTICIPANT_BY_IDEVENT);
        ps.setInt(1, idevent);
        ResultSet rs = ps.executeQuery();
         ObservableList<Participant> event = FXCollections.observableArrayList();
      
        while (rs.next()) {
            event.add(mapResultsToParticipant(rs.getInt(1),rs.getInt(2),rs.getInt(3), rs.getString(4),rs.getString(5)));
        }
   if(event.size()==0){return true;}
   else{
       System.out.println("Le participant existe deja");
       return false;
   }
   }
  
}
