/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import database.DB;
import static database.DB.getInstance;
import entities.Evenement;
import java.io.FileInputStream;
import java.sql.Blob;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author debba
 */
public class ServiceEvent {

    Connection connection;
    DB db = getInstance();

    private final String CREATE_EVENEMENT = "INSERT INTO evttesting(nom,type,date,lieu,description,heure,img,idM) VALUES(?,?,?,?,?,?,?,?)";
    private final String GET_EVENEMENT_BY_ID = "SELECT id,nom,type,date,lieu,description,heure,img,idM FROM evttesting WHERE id=?";
  //  private final String GET_ALL_EVENEMENT = "SELECT *FROM evttesting";
    private final String DELETE_EVENEMENT_BY_ID = "DELETE FROM evttesting WHERE id=?";
    private final String UPDATE_EVENEMENT = "UPDATE evttesting SET nom=?,type=?,date=?,lieu=?,description=?,heure=?,idM=? WHERE id=?";
     private final String GET_EVENEMENT_BY_NOM = "SELECT id,nom,type,date,lieu,description,heure,img FROM evttesting WHERE nom=?";
         private final String GET_ALL_EVENEMENT = "SELECT nom,type,date,lieu,description,heure FROM evttesting";


    public ServiceEvent() {
    connection=db.getConnection();
    }
// public void createEvenement(String nom, String type, Date date, String lieu, String description, String heure) throws SQLException {
//        PreparedStatement ps = connection.prepareStatement(CREATE_EVENEMENT);
//        ps.setString(1,nom);
//         ps.setString(2, type);
//          ps.setDate(3,date);
//         ps.setString(2,lieu);
//          ps.setString(2, description);
//           ps.setString(2,heure);
//        ps.executeUpdate();
//        System.out.println("EVENT created");
//    }
 public void createEvenement(Evenement e) throws SQLException {
        
     PreparedStatement ps = connection.prepareStatement(CREATE_EVENEMENT);
        ps.setString(1,e.getNom());
         ps.setString(2,e.getType());
          ps.setDate(3,e.getDate());
         ps.setString(4,e.getLieu());
          ps.setString(5,e.getDescription());
           ps.setString(6,e.getHeure());
           ps.setString(7,e.getImg());
           ps.setInt(8,e.getIdM());
         
        ps.executeUpdate();
        System.out.println("EVENT created");
    }
   
//    public void createEvenement(int id, String nom, Date date, double cap) throws SQLException {
//        PreparedStatement ps = connection.prepareStatement(CREATE_EVENEMENT);
//        ps.setInt(1, id);
//        ps.setString(2, nom);
//        ps.setDate(3, date);
//        ps.setDouble(4, cap);
//      
//
//        ps.executeUpdate();
//        System.out.println("EVENT created");
//    }

//   
    public Evenement mapResultsToEvenement(int id,String nom, String type, Date date, String lieu, String description, String heure,String img) {
         return new Evenement(id,nom,type,date,lieu,description,heure,img);
    }
  public ObservableList<Evenement> getAll()throws SQLException {
          ObservableList<Evenement> events = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement(GET_ALL_EVENEMENT);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            events.add(new Evenement( rs.getString(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getString(5),rs.getString(6)));
      }
 System.out.println("la liste des evenements:");
        return events;
    }
  
    public Evenement getEvenementById(int id)throws SQLException {
          PreparedStatement ps = connection.prepareStatement(GET_EVENEMENT_BY_ID);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
         System.out.println("l'evenement correspondant est:");
        return mapResultsToEvenement(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getString(6),rs.getString(7),rs.getString(8));
    }

 
//    public ObservableList <Evenement> getAll() throws SQLException {
//          ObservableList<Evenement> events = FXCollections.observableArrayList();
//        PreparedStatement ps = connection.prepareStatement(GET_ALL_EVENEMENT);
//        ResultSet rs = ps.executeQuery();
//        while (rs.next()) {
//            events.add(mapResultsToEvenement(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getString(6),rs.getString(7),rs.getString(8)));
//        }
// System.out.println("la liste des evenements:");
//        return events;
//    }

 
    public void deleteEvenementById(int id) throws SQLException{
       PreparedStatement ps = connection.prepareStatement(DELETE_EVENEMENT_BY_ID);
        ps.setInt(1, id);
        ps.executeUpdate();
        System.out.println("event deleted");
    }

   public  void MajEvent(Evenement p, int id)throws Exception {

        try {

            String req = "UPDATE `evttesting` SET `nom`=?,`type`=?,`date`=?,`lieu`=?,`description`=?,`heure`=?,`idM`=? WHERE id=?";

 PreparedStatement ste = connection.prepareStatement(req);
            
               ste.setString(1,p.getNom());
       ste.setString(2,p.getType());
     ste.setDate(3,p.getDate());
     ste.setString(4,p.getLieu());
     ste.setString(5,p.getDescription());
     ste.setString(6,p.getHeure());
     ste.setInt(7,p.getIdM());
            ste.setInt(8, id);
            System.out.println(id);
            ste.executeUpdate();

        } catch (SQLException ex) {
           
        }
    }
   
    public void updateEvenement(Evenement e)throws Exception {
         PreparedStatement ps = connection.prepareStatement(UPDATE_EVENEMENT);

        ps.setString(1,e.getNom());
       ps.setString(2,e.getType());
     ps.setDate(3,e.getDate());
     ps.setString(4,e.getLieu());
     ps.setString(5,e.getDescription());
     ps.setString(6,e.getHeure());
     ps.setInt(7,e.getIdM());
      ps.setInt(8,e.getId());

        ps.executeUpdate();
        System.out.println("updated ");
    }
  public void DelEvent(int id) {

        try {
            String req = "DELETE FROM evttesting  WHERE id=? ";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException ex) {
            
        }
    }
// 
//    public Evenement getEvenementByNom(String nom) throws SQLException {
//           PreparedStatement ps = connection.prepareStatement(GET_EVENEMENT_BY_NOM);
//        ps.setString(1, nom);
//        ResultSet rs = ps.executeQuery();
//        rs.next();//next return boolean
//         System.out.println("l'evenement correspondant est:");
//        return mapResultsToEvenement(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getDouble(4));
//    }
//  public ObservableList<Evenement> SearchByName(String nom)throws SQLException {
//        ObservableList<Evenement> event = FXCollections.observableArrayList();
//      PreparedStatement ps = connection.prepareStatement(GET_EVENEMENT_BY_NOM);
//         ps.setString(1, nom);
//        ResultSet rs = ps.executeQuery();
// rs.next();
//     ObservableList<Evenement> list =getAll();
//      while (rs.next()) {
//       list.add(mapResultsToEvenement(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getDouble(4)));
//      }
// System.out.println("la liste de mes evenements:");
//        return list;
//
//}    
}
