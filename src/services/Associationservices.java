/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import database.DB;
import static database.DB.getInstance;

import Utils.session;
import entities.Association;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author med
 */
public class Associationservices {

    Connection connection;
    DB db = getInstance();
    private final String Create_Association = "insert into association(nom_association,adresse,capital,chef_id,image) VALUES(?,?,?,?,?)";
    private final String GET_Association_By_ID = "select id_association, nom_association, date_creation, adresse, capital, chef_id  from  association where id_association=?";
    private final String GET_All_Associations = "select id_association, nom_association, date_creation, adresse, capital, chef_id   from association";
    private final String GET_All_USER_Associations = "select id_association, nom_association, date_creation, adresse, capital, chef_id   from association where chef_id = ?";
    private final String Delete_Association_BY_ID = "delete from association where id_association=?";
    private final String Udate_Association = "update association set nom_association=?,adresse=?,capital=?,image=? where id_association=?";

    public Associationservices() {
        connection = db.getConnection();
    }

    public int createAsso(Association a) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(Create_Association);
        PreparedStatement psid = connection.prepareStatement("select last_insert_id()");
        ps.setString(1, a.getNom_association());
        ps.setString(2, a.getAdresse());
        ps.setDouble(3, a.getCapital());
        ps.setInt(4, a.getId_chef());
        ps.setBinaryStream(5, a.getImage());

        ps.executeUpdate();
        ResultSet rs = psid.executeQuery();
        rs.next();
        int id = rs.getInt(1);
        System.out.println("association creer");

        return id;
    }

    public void addadherent(int id_asso, int id_m) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("INSERT INTO demande(idA,idM,etat) VALUES(?,?,?)");
        ps.setInt(1, id_asso);
        ps.setInt(2, id_m);
        ps.setString(3, "valider");

        ps.executeUpdate();
        System.out.println("adherent ajouter");

    }

    private Association ResultsToassAssociation(int id, String nom, Date date_creation, String adresse, double capital, int idchef) {
        return new Association(id, nom, date_creation, adresse, capital, idchef);

    }

    public Association getAssociationById(int id) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(GET_Association_By_ID);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return ResultsToassAssociation(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(2), rs.getDouble(4), rs.getInt(5));
    }

    public int getidAssoByName(String s) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SELECT id_association FROM association WHERE nom_association=?");
        ps.setString(1, s);
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return rs.getInt(1);
    }

    public ObservableList<Association> getAllassociations() throws SQLException {
        ObservableList<Association> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement(GET_All_Associations);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            a.add(ResultsToassAssociation(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getDouble(5), rs.getInt(6)));
        }

        return a;
    }

    public ObservableList<Association> SearshInAllAssociations(String nom) throws SQLException {
        ObservableList<Association> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("select id_association, nom_association, date_creation, adresse, capital, chef_id  "
                + ""
                + " FROM association WHERE nom_association LIKE ?");
        ps.setString(1, "%" + nom + "%");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            a.add(ResultsToassAssociation(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getDouble(5), rs.getInt(6)));
        }

        return a;
    }

    public ObservableList<Association> getAllUserOwnedAssociations(int idchef) throws SQLException {
        ObservableList<Association> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement(GET_All_USER_Associations);
        ps.setInt(1, idchef);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            a.add(ResultsToassAssociation(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getDouble(5), rs.getInt(6)));
        }

        return a;
    }

    public ObservableList<Association> SearshUserAssociations(String s, int idchef) throws SQLException {
        ObservableList<Association> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("select id_association, nom_association, date_creation, adresse, capital, chef_id  FROM association "
                + " WHERE chef_id =? AND nom_association LIKE ?");
        ps.setInt(1, idchef);
        ps.setString(2, "%" + s + "%");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            a.add(ResultsToassAssociation(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getDouble(5), rs.getInt(6)));
        }

        return a;
    }

    public ObservableList<String> getAllAssociationusers(int idasso) throws SQLException {
        ObservableList<String> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("SELECT m.nomutilisateur ,a.date_entree , m.email FROM demande a INNER JOIN membre m ON m.idM = a.idM WHERE id_association =?");
        ps.setInt(1, idasso);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            a.add(rs.getString(1) + ", membre depuis le :" + rs.getDate(2) + " email :" + rs.getString(3));
        }

        return a;
    }

    public ObservableList<String> getAllUserAssociations(int idasso) throws SQLException {
        ObservableList<String> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("SELECT ass.nom_association ,a.date_entree FROM demande a INNER JOIN association ass ON ass.id_association = a.idA WHERE idM =?");
        ps.setInt(1, idasso);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            a.add(rs.getString(1) + ", rejoint depuis le :" + rs.getDate(2));
        }

        return a;
    }

    public Image getAssociationImageByID(int idasso) throws SQLException, FileNotFoundException, IOException {
        PreparedStatement ps = connection.prepareStatement("SELECT image FROM association WHERE id_association = ?");
        ps.setInt(1, idasso);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            InputStream is = rs.getBinaryStream("image");
            OutputStream os = new FileOutputStream(new File("photo.jpg"));
            byte[] content = new byte[1024];
            int size = 0;
            while ((size = is.read(content)) != -1) {
                os.write(content, 0, size);
            }
            os.close();
            is.close();

        }
        Image image = new Image("file:photo.jpg");
        return image;
    }

    public ObservableList<ImageView> getAllAssociationImages() throws SQLException, FileNotFoundException, IOException {
        ObservableList<ImageView> img = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("SELECT image FROM association");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            InputStream is = rs.getBinaryStream("image");
            OutputStream os = new FileOutputStream(new File("photo.jpg"));
            byte[] content = new byte[1024];
            int size = 0;
            while ((size = is.read(content)) != -1) {
                os.write(content, 0, size);
            }
            os.close();
            is.close();
            Image image = new Image("file:photo.jpg");
            ImageView iv = new ImageView(image);
            img.add(iv);
        }
        return img;
    }

    public ObservableList<ImageView> getAllSearshedAssociationImages(String nom) throws SQLException, FileNotFoundException, IOException {
        ObservableList<ImageView> img = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("SELECT image FROM association WHERE nom_association LIKE ?");
        ps.setString(1, "%" + nom + "%");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            InputStream is = rs.getBinaryStream("image");
            OutputStream os = new FileOutputStream(new File("photo.jpg"));
            byte[] content = new byte[1024];
            int size = 0;
            while ((size = is.read(content)) != -1) {
                os.write(content, 0, size);
            }
            os.close();
            is.close();
            Image image = new Image("file:photo.jpg");
            ImageView iv = new ImageView(image);
            img.add(iv);
        }
        return img;
    }


    public void updateAssociation(Association a) {

        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(Udate_Association);
            ps.setString(1, a.getNom_association());
            ps.setString(2, a.getAdresse());
            ps.setDouble(3, a.getCapital());
            ps.setBinaryStream(4, a.getImage());

            ps.setInt(5, a.getId());

            ps.executeUpdate();
            System.out.println("updated ");
        } catch (SQLException ex) {
            Logger.getLogger(Associationservices.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteAssociationById(int id) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(Delete_Association_BY_ID);
        ps.setInt(1, id);
        ps.executeUpdate();
        System.out.println("deleted");

    }

    public boolean alreadyAdherent(int id_asso, int id_membre) throws SQLException {
        int count = 0;
        PreparedStatement ps = connection.prepareStatement("select count(*) from demande where idA=? and idM=?");
        ps.setInt(1, id_asso);
        ps.setInt(2, id_membre);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            count = rs.getInt(1);
        }
        if (count != 0) {
            return true;
        } else {
            return false;
        }

    }

    public boolean associationNameExist(String nomasso) throws SQLException {
        int count = 0;
        PreparedStatement ps = connection.prepareStatement("select count(*) from association where nom_association=?");
        ps.setString(1, nomasso);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            count = rs.getInt(1);
        }
        if (count != 0) {
            return true;
        } else {
            return false;
        }

    }

    public void deleteAdherent(int idasso, int idm) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("DELETE FROM demande WHERE idA=? AND idM=?");
        ps.setInt(1, idasso);
        ps.setInt(2, idm);
        ps.executeUpdate();
        System.out.println("no longer adherent");

    }

    public void addToRegister(String s) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("INSERT INTO registre (contexte) VALUE (?)");
        ps.setString(1, s);
        ps.executeUpdate();
    }
    public int countAdherent() throws SQLException{
        int count = 0;
        PreparedStatement ps = connection.prepareStatement("SELECT COUNT(DISTINCT idM) FROM demande");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            count = rs.getInt(1);
        }
        return count;
    }
    
    public ObservableList<String> getFeedbacks() throws SQLException{
        ObservableList<String> a = FXCollections.observableArrayList();
        PreparedStatement ps = connection.prepareStatement("select contexte from registre order by date desc");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            a.add(rs.getString(1));
        }
        return a;
    }

}
