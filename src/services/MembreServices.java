/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Utils.session;

import database.DB;
import static database.DB.getInstance;

import entities.Membre;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author med
 */
public class MembreServices {

    Connection connection;
    DB db = getInstance();
    private final String GET_USER_BY_username = "SELECT * FROM membre WHERE username=?";
    private final String CREATE_USER = "INSERT INTO membre(nom,prenom,sexe,datedenaissance,email,email_canonical,adresse,telephone,username,username_canonical,password,grade,enabled,roles) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String GET_USER_BY_ID = "SELECT * FROM membre WHERE id=?";
    private final String GET_ID_USER_BY_username = "SELECT id FROM membre WHERE username=?";
    private final String GET_ALL_USER = "SELECT *FROM membre";
    //    private final String DELETE_USER_BY_ID="DELETE FROM membre WHERE id=?";
    private final String UPDATE_USER = "UPDATE membre SET nom=?,prenom=?,sexe=?,datedenaissance=?,email=?,adresse=?,telephone=?,password=?,grade=? WHERE id=?";
    private final String GET_USER_BY_NOM = "SELECT * FROM membre WHERE nom=?";

    public MembreServices() {
        connection = db.getConnection();
    }

    public void createUSER(Membre m) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(CREATE_USER);
        ps.setString(1, m.getNom());
        ps.setString(2, m.getPrenom());
        ps.setString(3, m.getSex());
        ps.setDate(4, m.getDatenaissance());
        ps.setString(5, m.getEmail());
        ps.setString(6, m.getEmail());
        ps.setString(7, m.getAdresse());
        ps.setInt(8, m.getTelephone());
        ps.setString(9, m.getNomutilisateur());
        ps.setString(10, m.getNomutilisateur());
        ps.setString(11, m.getMotdepasse());
        ps.setString(12, m.getGrade());
        ps.setInt(13, 1);
        ps.setString(14, "a:0:{}");

        ps.executeUpdate();
        System.out.println("User added");
    }

    private Membre mapResultsToUSER(int id, String nom, String prenom, String sex, Date datenaissance, String email, String adresse, int telephone, String nomutilisateur, String motdepasse, String grade) {
        return new Membre(id, nom, prenom, sex, datenaissance, email, adresse, telephone, nomutilisateur, motdepasse, grade);
    }

    public Membre getUSERById(int id) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(GET_USER_BY_ID);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return mapResultsToUSER(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
    }

    public int getidUSERByusername() throws SQLException {
        PreparedStatement ps = connection.prepareStatement(GET_ID_USER_BY_username);
        ps.setString(1, session.getUser());
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return rs.getInt(1);
    }
    public Membre getUSERByusername() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("select id,nom,prenom,sexe,datedenaissance,email,adresse,telephone,username,password,grade from membre where username=?");
        ps.setString(1, session.getUser());
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return mapResultsToUSER(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
    }
    
    public String getEmailUSERByusername() throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SELECT email FROM membre WHERE username=?");
        ps.setString(1, session.getUser());
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return rs.getString(1);
    }

    public List<Membre> getAllMembres() throws SQLException {
        List<Membre> event = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement(GET_ALL_USER);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            event.add(mapResultsToUSER(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11)));
        }
        System.out.println("La liste des membres: ");
        return event;
    }

    public void updateUSER(Membre m) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(UPDATE_USER);

        ps.setString(1, m.getNom());
        ps.setString(2, m.getPrenom());
        ps.setString(3, m.getSex());
        ps.setDate(4, m.getDatenaissance());
        ps.setString(5, m.getEmail());
        ps.setString(6, m.getAdresse());
        ps.setInt(7, m.getTelephone());
        ps.setString(9, m.getMotdepasse());
        //ps.setString(10, grade);
        ps.setInt(11, m.getId());

        ps.executeUpdate();
        System.out.println("l'utilisateur de nom:" + " " + m.getNom() + " est " + "mis à jour");
    }

    public Membre getUSERByNOM(String nom) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(GET_USER_BY_NOM);
        ps.setString(1, nom);
        ResultSet rs = ps.executeQuery();
        rs.next();//next return boolean
        return mapResultsToUSER(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
    }

    public boolean usernameExist(String username) throws SQLException {
        int count = 0;
        PreparedStatement ps = connection.prepareStatement("select count(*) from membre where username = ?");
        ps.setString(1, username);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            count = rs.getInt(1);
        }
        if (count != 0) {
            return true;
        } else {
            return false;
        }

    }
    
    public int countMembres() throws SQLException{
        int count = 0;
        PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM membre");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            count = rs.getInt(1);
        }
        return count;
    }
   

}
