/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import database.DB;
import entities.Catégorie;
import entities.Commande;

import entities.Panier;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author brini
 */
public class PanierService 

{
    Connection cnx;
    Statement statement;
    ResultSet result;

    public PanierService() 
    {
        cnx = DB.getInstance().getConnection();
        try
        {
            statement = cnx.createStatement();
        } catch (SQLException ex)
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void create(Panier c) 
       
    {
        
//         int id = 0;
        try
        {
            String request = "INSERT INTO `panier` ( `idcommande`, `idstock`, `quantite`, `prix`,total) VALUES ( '"+c.getIdcmd()+"', '"+c .getIdcat()+"', '"+c.getQuantitedes()+"', '"+c.getPrix()+"', '"+c.getTotalprix()+"')";
            statement.executeUpdate(request);
//            String reqid = "select last_insert_id()";
//            result = statement.executeQuery(reqid);
//            result.next();
//            id = result.getInt(1);

        } catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
       

    }
    
public ObservableList<Panier> getALLCommandes(int idM) 
{
        String etat = "0";
        ObservableList<Panier> resultat = FXCollections.observableArrayList();
        try
        {
            statement = cnx.createStatement();
            result = statement.executeQuery("select s.id_cat, s.description ,p.idcommande,p.quantite ,p.prix ,p.total  from stock s inner join panier p on s.id_cat =p.idstock inner join commande c on c.id_cmd = p.idcommande and c.etat="+etat+" and  c.id_user = "+idM);
        
            while (result.next())
            {


                
                Panier c = new Panier();
              
                
                c.setIdcat(result.getInt(1));
                c.setDescription(result.getString(2));
                
                c.setIdcmd(result.getInt(3));
                c.setQuantitedes(result.getDouble(4));
                c.setPrix(result.getDouble(5));
                c.setTotalprix(result.getDouble(6));
               
                

                resultat.add(c);

            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
          System.out.println(resultat);
        return resultat;
    }
      
      
      
      
      
       public List<Panier> getALLCommandesSync(int idM) 
      {
       List<Panier> resultat = new ArrayList();
       String etat ="0";
        try
        {
            statement = cnx.createStatement();
            result = statement.executeQuery("select s.id_cat, s.description ,p.idcommande,p.quantite ,p.prix ,p.total  from stock s inner join panier p on s.id_cat =p.idstock inner join commande c on c.id_cmd = p.idcommande and c.etat="+etat+" and  c.id_user = "+idM);
        
            while (result.next())
            {


                
                Panier c = new Panier();
              
                
                c.setIdcat(result.getInt(1));
                c.setDescription(result.getString(2));
                
                c.setIdcmd(result.getInt(3));
                c.setQuantitedes(result.getDouble(4));
                c.setPrix(result.getDouble(5));
                c.setTotalprix(result.getDouble(6));
               
                

                resultat.add(c);
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
          System.out.println(resultat);
        return resultat;
    }
      
        public boolean deleteCommande(int id)
    { 
       
       
        String req = "delete from panier where panier.idstock="+id;
       
        
            try 
            {
               statement = cnx.createStatement();
                statement.executeUpdate(req);
                return true ;
            } catch (SQLException ex)
            {
                Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        return false ;
    }
        
       public void UpdateCommande(int id_cat, double quantite,double prix ,double total) 
      {
          String req ="update panier set quantite = "+quantite+" ,prix = "+prix+" , total ="+total+" where panier.idStock= "+id_cat;
        try
        {
            statement = cnx.createStatement();
           statement.executeUpdate(req);
        
           
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }

      
      }
       
     
       
       public Boolean ExistCategorie (int idCat,int idCmd) 
     {
         String req = "select  idstock from panier where  (idstock = "+idCat+" and idcommande = "+idCmd+")" ; 
        try {
           statement = cnx.createStatement();
           result = statement.executeQuery(req);
            while (result.next())
                
            {
              int a = result.getInt(1);
              
              statement.close();
                      
                return true ;
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
           
        return false ;
     }
       
      public void UpdateCommandePanier( Panier c) 
      {
          String req ="update panier set quantite = "+c.getQuantitedes()+" , total = "+c.getTotalprix()+" where  idstock= "+c.getIdcat() ;
//         
        try
        {
            statement = cnx.createStatement();
           statement.executeUpdate(req);
        
           
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }

      
     }
        
//           public boolean EnregistrerCommande(int id)
//    { 
//       
//       
//        String req = "delete   from commande where id_membre="+id;
//       
//        
//            try 
//            {
//               statement = cnx.createStatement();
//                statement.executeUpdate(req);
//                return true ;
//            } catch (SQLException ex)
//            {
//                Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        
//        return false ;
//    }
        public Boolean ExistCommande(int idM )
       {
           String etat = "0" ;
              String req = "select  id_cmd from commande where  (etat = "+etat+" and id_user = "+idM +")" ;  
               try {
           statement = cnx.createStatement();
           result = statement.executeQuery(req);
            while (result.next())
                
            {
              int a = result.getInt(1);
              
              statement.close();
                      
                return true ;
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
           
        return false ;
           
       }
        
      public int createCommande(Commande c)
        {
          int id = 0 ;  
               try
        {
            String request = "INSERT INTO `commande` (  `id_user`, `etat`) VALUES (  '"+c.getId_user()+"', '"+c.getEtat()+"')";
            statement.executeUpdate(request);
                        String reqid = "select last_insert_id()";
            result = statement.executeQuery(reqid);
            result.next();
            id = result.getInt(1);

        } catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
       return id ;
        }
        
         public int IdCmd(int idM) {
           int id = 0 ;
           String etat = "0";
           String req = "select id_cmd from commande where etat = "+etat+" and id_user = "+idM ;
        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {
               id = result.getInt("id_cmd");
                System.out.println(id);
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }
         
     public double Qte(int id, int idM )
    {
        double qte = 0.0;
        String etat ="0";
        String req = "select p.quantite from panier p inner join commande c on p.idcommande = c.id_cmd where p.idstock =" + id+" and id_user = "+idM +" and etat = "+etat ;
        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {
                qte = result.getDouble(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return qte;
    }
     
     
      public void UpdateTotal( int id , Double Total) 
      {
          String req ="update commande set total = "+Total+" where  id_cmd= "+id ;
//         
        try
        {
            statement = cnx.createStatement();
           statement.executeUpdate(req);
        
           
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }

      
     }
      
      
      
        public double total(int id, int idM )
    {
        double total = 0.0;
        String etat ="0";
        String req = "select total from  commande  where id_cmd =" + id+" and id_user = "+idM +" and etat = "+etat ;
        try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {
               total = result.getDouble(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return total;
    }
        
        public  void valideCommande(int idcmd , int idM)
        {
            String etat = "1";
                 
            
               String req ="update commande set etat = "+etat+" where  id_cmd= "+idcmd+" and id_user = "+idM;
//         
        try
        {
            statement = cnx.createStatement();
           statement.executeUpdate(req);
        
           
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        }
    public ObservableList<Commande> getALLMesCommandes(int idM) 
{
        String etat = "1";
        ObservableList<Commande> resultat = FXCollections.observableArrayList();
        try
        {
            statement = cnx.createStatement();
            result = statement.executeQuery("select id_cmd ,total , date  commande  where etat="+etat+" and  id_user = "+idM);
        
            while (result.next())
            {


                
               Commande c = new Commande();
              
                
                c.setIdCmd(result.getInt(1));
                  c.setTotal(result.getFloat(2));
                
                
                c.setDate(result.getString(3));
         
               
                

                resultat.add(c);

            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
        }
          System.out.println(resultat);
        return resultat;
    }
    
     public Date  SelectedDate(int id)
     {
         String etat ="1";
         String req = "select date from commande  where id_user ="+id+" and etat = "+etat ;
         Date date = null ; 
           try {
            statement = cnx.createStatement();
            result = statement.executeQuery(req);
            while (result.next()) {

                date  = result.getDate("date");
               
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return date ;
         
     }
     
         public void delete(int id , int idm)
    { 
       String etat = "0";
       
        String req = "delete from commande where commande.id_cmd="+id+" and commande.etat = "+etat+" and commande.id_user = "+idm;
       
        
            try 
            {
               statement = cnx.createStatement();
                statement.executeUpdate(req);
             
            } catch (SQLException ex)
            {
                Logger.getLogger(PanierService.class.getName()).log(Level.SEVERE, null, ex);
            }
  
    }
      
}
